﻿using Abilities;
using GameplayEntities;
using LLHandlers;
using LLScreen;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedTraining
{
    class TrainingPartner : MonoBehaviour
    {
        public static TrainingPartner Instance { get; set; } = null;

        PlayerEntity PartnerEntity { set; get; }
        PlayerEntity PlayerEntity { set; get; }
        BallEntity Ball { set; get; }

        ALDOKEMAOMB Partner { set; get; }
        ALDOKEMAOMB Player { set; get; }
        HPNLMFHPHFD LobbySingle { set; get; }
        GetHitPlayerEntity GetHitPlayerEntity { set; get; }

        private void Start()
        {
            //OGONAGCFDPK == GameStatesGame
            //DNPFJHMAIBP.GKBNNFEAJGO(Msg.GAME_PAUSE, 0);
            //DNPFJHMAIBP.GKBNNFEAJGO(Msg.START, 0);
            Player = ALDOKEMAOMB.BJDPHEHJJJK(0);
            AddPlayer(1);
            // OGONAGCFDPK.instance.KIAKGLIBMCG(); // DespawnBall OGONAGCFDPK == GameStatesGame
            // OGONAGCFDPK.instance.ENNNJNLANCO(true, 0); // Respawn Players
            //  BallHandler.instance.SpawnBall(0);
            // BG.SetState(BGState.NORMAL, true);
            AudioHandler.PlaySfx(Sfx.MENU_CONFIRM);
        }


        void OnDestroy()
        {
            DebugSettings.instance.dontLoseHP = false;
            DebugSettings.instance.dontGetHit = false;
            DebugSettings.instance.noEnergyRequiredForSpecial = false;
            Player.GDEMBCKIDMA = Controller.all;
            ClearInput(Partner);
            Partner.GDEMBCKIDMA = Controller.none;
            Partner.EKNFACPOJCM(false); // Join Match
            Partner.FMCGJNPNIPH(); //Player.Reset
            AudioHandler.PlaySfx(Sfx.LOBBY_PLAYER_OUT);
            AdvancedTraining.Log.LogDebug("AdvancedTraining: TrainingPartner Destroyed");
            ScreenPlayers screen = FindObjectOfType<ScreenPlayers>();
            screen?.SetPlayerLayout(LobbyPlayerLayout.ONE_WITH_DESC_SPACE);
            screen?.lbCharInfo.gameObject.SetActive(true);
            //partner.LALEEFJMMLH = partner.DOFCCEDJODB = Character.NONE; //Character - Character Selected
            //player.CHNGAKOIJFE = false;
            //screen.playerSelections[1].SetCharacterNone();
            //screen.playerSelections[1].SetStateNoPlayer();
        }

        bool hasAimed = false;
#if DEBUG
        readonly bool chkSpecial = false;
#endif
        bool chkParry = false;
        bool disableMove = false;
        bool noGravity = false;
        bool continousAction = false;
        SwingType swingTypeId = SwingType.SWING;
        AngleMode angleModeId = AngleMode.RANDOM;
        GetUpType getUpTypeId = GetUpType.NONE;
        int parryChance = 10;
        int parryFrame = 10;
        HHBCPNCDNDH maxGravDefault;
        void AddPlayer(int pNr)
        {
            if (ALDOKEMAOMB.BJDPHEHJJJK(pNr) != null || ALDOKEMAOMB.BJDPHEHJJJK(0) != null)
            {
                Partner = ALDOKEMAOMB.BJDPHEHJJJK(pNr);
                LobbySingle = FindObjectOfType<HFAEJNGHDDM>();
                ScreenPlayers screen = FindObjectOfType<ScreenPlayers>();
                screen.SetPlayerLayout(LobbyPlayerLayout.TWO);
                screen.lbCharInfo.gameObject.SetActive(false);
                Partner.EKNFACPOJCM(true); // Join Match
                Partner.CHNGAKOIJFE = true; // Selected
                Partner.DOFCCEDJODB = Character.RANDOM; // characterSelected
                bool matchingCharacter = Player.DOFCCEDJODB == Partner.DOFCCEDJODB && Partner.DOFCCEDJODB != Character.RANDOM && Partner.DOFCCEDJODB != Character.NONE;
                bool matchingVariant = Player.AIINAIDBHJI == CharacterVariant.DEFAULT;
                Partner.AIINAIDBHJI = (matchingCharacter && matchingVariant) ? CharacterVariant.ALT0 : CharacterVariant.DEFAULT; // Character Variant
                LobbySingle.GNCDBOBHOHN(Partner); // Add Player
                LobbySingle.BDMIDGAHNLA(Partner); // UpdatePlayer bool: Play_Selection_Anim
                AudioHandler.PlayAnnounce(Partner.DOFCCEDJODB);
                AudioHandler.PlaySfx(Sfx.LOBBY_PLAYER_IN);
                #region Notes
                //player2.CJFLMDNNMIE = pNr; // Player Index
                //partner.ALBOPCLADGN = true; // isAi
                //partner.INPJBIFEPMB = 0; // AI Level
                //partner.MLFHMGNCMNA(GameMode.TRAINING, true); //Reset Team
                //partner.LALEEFJMMLH = Character.KID; // Character
                //partner.CHNGAKOIJFE = player.CHNGAKOIJFE = false; // Selected?
                //partner.AIINAIDBHJI = CharacterVariant.MODEL_ALT2; // Character Variant
                //player2.OKDEILOGKFB(); // SetSpectator
                //partner.GFCMODHPPCN = false; // Ready
                //player2.GAFCIHKIGNM = true; // isLocal
                //partner.KLEEADMGHNE = Multiplayer.P2P.localHost;
                //player.OLNANNOFOJO = pNr; // CPU Selecting
                #endregion

            }
        }

        AngleMode previousAngleMode;
        //int previousInput;
        float coolDown;
        bool noHitPartner = false;

        void Update()
        {
            if (AdvancedTraining.IsOnline && AdvancedTraining.CurrentGameMode != GameMode.TRAINING)
            {
                AdvancedTraining.Log.LogError($"\"TrainingPartner\" detected online Terminationg LLB");
                AudioHandler.PlaySfx(Sfx.KILL);
                Application.Quit();
                return;
            }

            if (!AdvancedTraining.InGame) { return; }

            if (PartnerEntity == null && Partner.JCCIAMJEODH != null)
            {
                PartnerEntity = Partner.JCCIAMJEODH;
                maxGravDefault = PartnerEntity.maxGravity;
                if (GetHitPlayerEntity == null)
                {
                    GetHitPlayerEntity = FindObjectOfType<GetHitPlayerEntity>();
                }
            }

            if (Partner.ALBOPCLADGN == true)
            {
                return;
            }


            if (PlayerEntity == null && Player.JCCIAMJEODH != null) PlayerEntity = Player.JCCIAMJEODH;
            if (World.instance != null && Ball == null) Ball = BallHandler.instance.GetBall(0);

            if (ALDOKEMAOMB.BJDPHEHJJJK(1).JCCIAMJEODH != null || ALDOKEMAOMB.BJDPHEHJJJK(0).JCCIAMJEODH != null)
            {

#if DEBUG
                FrameReplay();
#endif

                if (InputHandler.GetInput(Player, InputAction.DOWN) && coolDown == 0)
                {
                    if (InputHandler.GetInput(Player, InputAction.EXPRESS_LEFT) && angleModeId != AngleMode.RANDOM)
                    {
                        coolDown = Time.deltaTime;
                        angleModeId--;
                    }
                    if (InputHandler.GetInput(Player, InputAction.EXPRESS_RIGHT) && angleModeId != AngleMode.BACK)
                    {
                        coolDown = Time.deltaTime;
                        angleModeId++;
                    }
                }

                coolDown = coolDown > 0 && coolDown < 0.1f ? coolDown += Time.deltaTime : coolDown = 0;
                if (Input.GetKeyDown(AdvancedTraining.Instance.SwitchToOtherPlayer.Value))
                {
                    disableMove = !disableMove;
                    Player.GDEMBCKIDMA = disableMove ? Controller.none : Controller.all;
                    Partner.GDEMBCKIDMA = !disableMove ? Controller.none : Controller.all;
                    ClearInput(Player);
                    ClearInput(Partner);
                    AudioHandler.PlaySfx(Sfx.MENU_SCROLL);
                }

                if (!disableMove && swingTypeId != SwingType.NONE)
                {
                    if (continousAction)
                    {
                        ContinousAction(PartnerEntity);
                    }
                    else
                    {
                        ActivateHitbox(PartnerEntity);
                    }
                }

                if (!disableMove)
                {
                    GroundRecover(PartnerEntity);
                }

                HitableEntity hitEntity = GetHittingEntity();
                if ((PartnerEntity.IsInHitpause() && (hitEntity.InBuntTypeStun() || hitEntity.InHitstunByAPlayer())) || hitEntity.hitableData.lastHitterIndex == 1 && hitEntity.hitableData.hitstunState == HitstunState.PITCH_STUN)
                {
                    AngleMode mode = angleModeId;
                    if (swingTypeId != SwingType.NONE && !hasAimed)
                    {
                        GetAim(PartnerEntity, mode);
                        hasAimed = true;
                    }

                    if (HHBCPNCDNDH.HPLPMEAOJPM(PartnerEntity.RemainingHitstunTime(), ConvertTo.FramesDuration60fps(parryFrame + 2)) && !chkParry && trainPartBtn[0])
                    {

                        if (Random.Range(1, 100 + 1) <= parryChance * 10)
                        {
                            InputHandler.SetInput(PartnerEntity.player, InputAction.BUNT, 100, true);
                        }
                        //partnerEntity.abilityData.bufferedParry = Random.Range(1, 100 + 1) <= parryChance * 10;
                        chkParry = true;
                    }
                    else if (chkParry && InputHandler.previousInput[PartnerEntity.playerIndex, InputAction.ActionToIndex(InputAction.BUNT)] == 100)
                    {
                        InputHandler.SetInput(PartnerEntity.player, InputAction.BUNT, 0, false);
                    }

                }
                else if (hasAimed && !PartnerEntity.IsInHitpause())
                {
                    ClearInput(Partner);
                    hasAimed = false;
                    chkParry = false;
                    //AdvancedTraining.Log.LogDebug("Partner's Inputs Cleared");
                }
                PartnerIntangable();
                PartnerGravity();
            }
        }

        HitableEntity GetHittingEntity()
        {
            HitableEntity hittingEntity = BallHandler.instance.GetBall(0);
            var corpses = ItemHandler.instance.corpseItems;

            if (hittingEntity.InHitstunByAPlayer() == false)
            {
                for (int i = 0; i < corpses.Length; i++)
                {
                    if (corpses[i].InBuntTypeStun() || corpses[i].InHitstunByAPlayer() || corpses[i].hitableData.lastHitterIndex == 1 && corpses[i].hitableData.hitstunState == HitstunState.PITCH_STUN)
                    {
                        hittingEntity = corpses[i]; break;
                    }
                }
            }

            return hittingEntity;
        }


#if DEBUG
        int sameFrame;
        void FrameReplay()
        {
            if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                AudioHandler.PlaySfx(Sfx.EXPRESS);
                startRecord = !startRecord;
                if (startRecord) World.instance.SaveState();
                if (startRecord) currentRecordedIns.Clear();
                //if (!startRecord) cleanReplay();
            }

            if (startRecord)
            {
                int[,] input = new int[4, InputAction.nGameActions];
                currentRecordedIns.Add(input);
                for (int i = 0; i < InputAction.nGameActions; i++)
                {
                    currentRecordedIns[currentRecordedIns.Count - 1][1, i] = InputHandler.currentInput[1, i];
                    //AdvancedTraining.Log.LogDebug($"StateSystem: {i} : {aTcurInput[0, i]}");
                }
            }

            if ((Input.GetKeyDown(KeyCode.Keypad8) || playReplay) && currentRecordedIns.Count > 0 && frameIncrment <= currentRecordedIns.Count)
            {
                if (OGONAGCFDPK.DDBJKEIHELD == sameFrame) return;

                sameFrame = OGONAGCFDPK.DDBJKEIHELD;
                if (!playReplay) { World.instance.LoadState(); }
                playReplay = true;
                for (int i = 0; i < InputAction.nGameActions; i++)
                {
                    InputHandler.currentInput[1, i] = currentRecordedIns[frameIncrment][1, i];
                    //InputHandler.SetInput(partner, InputAction.IndexToAction(i), currentRecordedIns[frameIncrment][1, i], InputHandler.previousInput[1, i] == 100 ? false : true);
                    //AdvancedTraining.Log.LogDebug($"StateSystem: {i} : {aTcurInput[0, i]}");
                }
                frameIncrment += 2;
            }
            else
            {
                frameIncrment = 0;
                playReplay = false;
            }
        }

        int pos = 0;
        void CleanReplay()
        {
            for (int i = 0; i < currentRecordedIns.Count; i += 2, pos++)
            {
                currentRecordedIns[pos] = currentRecordedIns[i];
            }
            currentRecordedIns.RemoveRange(pos, currentRecordedIns.Count - pos);
            pos = 0;
        }
#endif
        void ActivateHitbox(PlayerEntity player)
        {
            bool active = false;
            BallEntity ball = BallHandler.instance.GetBall();
            bool isNormal = player.abilityData.playerState == PlayerState.NORMAL;
            if (ball != null) active = (ball.ballData.ballState == BallState.STICK_TO_PLAYER || ball.ballData.ballState == BallState.BUNTED || ball.ballData.lastHitterIndex != player.playerIndex || ItemHandler.instance.CorpsesActive() > 0) && !player.playerData.hitstunState.ToString().Contains("HIT_BY_BALL") && !disableMove;
            foreach (KeyValuePair<string, PlayerHitbox> hitbox in player.hitboxes)
            {
                switch (swingTypeId)
                {
                    case SwingType.SWING:
                        hitbox.Value.active = active && hitbox.Key == "NEUTRAL_HITBOX" && isNormal;
                        break;
                    case SwingType.SMASH:
                        hitbox.Value.active = active && (hitbox.Key == "SMASH_HITBOX" || hitbox.Key == "SMASH_TOP_HITBOX") && isNormal;
                        break;
                    case SwingType.SPIKE:
                        hitbox.Value.active = active && hitbox.Key == "DOWN_AIR_HITBOX" && isNormal;
                        break;
                    case SwingType.BUNT:
                        hitbox.Value.active = active && (hitbox.Key == "BUNT_HITBOX" || hitbox.Key == "BUNT_HITBOX2") && isNormal;
                        break;
                    case SwingType.GRAB:
                        hitbox.Value.active = active && hitbox.Key == "GRAB_HITBOX" && isNormal;
                        break;
                    default:
                        hitbox.Value.active = false;
                        break;
                }
            }
            GetHitPlayerEntity.CheckParry();
        }
        void GroundRecover(PlayerEntity player)
        {
            if (player.GetCurrentAbility() == null || getUpTypeId == GetUpType.NONE) return;

            if (player.GetCurrentAbility().name.Contains("knockedOut"))
                switch (getUpTypeId)
                {
                    case GetUpType.STAND:
                        InputHandler.SetInput(player.player, (player.playerData.heading == Side.LEFT) ? InputAction.LEFT : InputAction.RIGHT, 100, true); break;
                    case GetUpType.BLAZE:
                        InputHandler.SetInput(player.player, InputAction.SWING, 100, true); break;
                    case GetUpType.BUNT:
                        InputHandler.SetInput(player.player, InputAction.BUNT, 100, true); break;
                    default:
                        return;
                };

            if (player.GetCurrentAbility().name.Contains("getUp"))
            {
                ClearInput(player.player);
            }
        }

        readonly string[] excludeAngle = new string[]
{
            "None",
            "Up",
            "Forward",
            "Down",
};
        readonly string[] excludeDairAngle = new string[]
{
            "None",
            "Forward",
            "Down",
            "Back",
};

        List<AngleMode> GetAvailableAngles()
        {
            List<AngleMode> angle = new List<AngleMode>()
            {
                AngleMode.UP,
                AngleMode.DOWN,
                AngleMode.FORWARD,
                AngleMode.BACK,
            };
            if (DownAir)
            {
                angle.Remove(AngleMode.UP);
            }
            else
            {
                angle.Remove(AngleMode.BACK);
            }
            return angle;
        }

        void ExcludeAngle(ref List<AngleMode> angle)
        {
            if (DownAir)
            {
                switch (excludedAngleSelected)
                {
                    case 1:
                        angle.Remove(AngleMode.FORWARD);
                        break;
                    case 2:
                        angle.Remove(AngleMode.DOWN);
                        break;
                    case 3:
                        angle.Remove(AngleMode.BACK);
                        break;
                }
            }
            else
            {
                switch (excludedAngleSelected)
                {
                    case 1:
                        angle.Remove(AngleMode.UP);
                        break;
                    case 2:
                        angle.Remove(AngleMode.FORWARD);
                        break;
                    case 3:
                        angle.Remove(AngleMode.DOWN);
                        break;
                }
            }
        }

        void GetAim(PlayerEntity p, AngleMode mode)
        {
            Ability ability = Partner.JCCIAMJEODH.GetCurrentAbility();
            bool downAir = ability != null && ability == Partner.JCCIAMJEODH.downAirSwingAbility;
            List<AngleMode> angle = GetAvailableAngles();
            switch (mode)
            {
                case AngleMode.RANDOM:
                    ExcludeAngle(ref angle);
                    mode = previousAngleMode = angle[Random.Range(0, angle.Count)];
                    break;
                case AngleMode.NO_REPEAT:
                    AngleMode preMode = previousAngleMode;
                    do
                    {
                        mode = previousAngleMode = angle[Random.Range(0, angle.Count)];
                    } while (previousAngleMode == preMode);
                    break;
                case AngleMode.CYCLE:
                    switch (previousAngleMode)
                    {
                        case AngleMode.UP:
                            mode = previousAngleMode = AngleMode.FORWARD; break;
                        case AngleMode.FORWARD:
                            mode = previousAngleMode = AngleMode.DOWN; break;
                        case AngleMode.DOWN:
                            mode = previousAngleMode = !downAir ? AngleMode.UP : AngleMode.BACK; break;
                        case AngleMode.BACK:
                            mode = previousAngleMode = !downAir ? AngleMode.DOWN : AngleMode.FORWARD; break;
                        default:
                            mode = previousAngleMode = downAir ? AngleMode.DOWN : AngleMode.FORWARD; break;
                    }
                    break;
            }

            int actionInt = InputAction.NONE;
            switch (mode)
            {
                case AngleMode.UP:
                    actionInt = InputAction.UP; break;
                case AngleMode.DOWN:
                    actionInt = InputAction.DOWN; break;
                case AngleMode.FORWARD:
                    actionInt = p.moveableData.heading == Side.RIGHT ? InputAction.RIGHT : InputAction.LEFT; break;
                case AngleMode.BACK:
                    actionInt = p.moveableData.heading == Side.RIGHT ? InputAction.LEFT : InputAction.RIGHT; break;
            }
            InputHandler.SetInput(p.player, actionInt, 100, true);
        }

        bool InSwingFollowThrough(PlayerEntity playerEnt)
        {
            string abilityName = playerEnt.abilityData.abilityState;
            switch (abilityName)
            {
                case "SWING_RETURN":
                case "DOWN_AIR_OUT":
                case "OUT_GRAB_NEW":
                case "BUNT_OUT":
                case "WALL_SWING_FOLLOW_THROUGH":
                case "SMASH_FOLLOW_THROUGH":
                case "PITCH_OUT":
                    return ConvertTo.Equal(ConvertTo.Subtract(playerEnt.GetAbilityState(abilityName).duration, ConvertTo.FramesDuration60fps(1)), playerEnt.abilityData.abilityStateTimer);
            }
            return false;
        }

        void ContinousAction(PlayerEntity playerEnt)
        {
            bool isNormal = playerEnt.playerData.playerState == PlayerState.NORMAL || playerEnt.playerData.playerState == PlayerState.SPECIAL;

            switch (swingTypeId)
            {
                case SwingType.SWING:
                    if (isNormal || InSwingFollowThrough(playerEnt))
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.SWING, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.SWING)] != 100);
                    }
                    else
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.SWING, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.SWING)] != 100);
                    }
                    break;
                case SwingType.SMASH:
                    int action = playerEnt.entityData.heading == Side.RIGHT ? InputAction.RIGHT : InputAction.LEFT;
                    if ((isNormal || InSwingFollowThrough(playerEnt)) && !playerEnt.OnGround())
                    {
                        InputHandler.SetInput(playerEnt.player, action, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.DOWN)] != 100);
                        InputHandler.SetInput(playerEnt.player, InputAction.SWING, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.SWING)] != 100);
                    }
                    else
                    {
                        InputHandler.SetInput(playerEnt.player, action, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.DOWN)] != 100);
                        InputHandler.SetInput(playerEnt.player, InputAction.SWING, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.SWING)] != 100);
                    }
                    break;
                case SwingType.SPIKE:
                    if ((isNormal || InSwingFollowThrough(playerEnt)) && !playerEnt.OnGround())
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.DOWN, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.DOWN)] != 100);
                        InputHandler.SetInput(playerEnt.player, InputAction.SWING, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.SWING)] != 100);
                    }
                    else
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.DOWN, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.DOWN)] != 100);
                        InputHandler.SetInput(playerEnt.player, InputAction.SWING, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.SWING)] != 100);
                    }
                    break;
                case SwingType.BUNT:
                    if (isNormal || InSwingFollowThrough(playerEnt))
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.BUNT, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.BUNT)] != 100);
                    }
                    else
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.BUNT, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.BUNT)] != 100);
                    }
                    break;
                case SwingType.GRAB:
                    if (isNormal || InSwingFollowThrough(playerEnt))
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.GRAB, 100, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.GRAB)] != 100);
                    }
                    else
                    {
                        InputHandler.SetInput(playerEnt.player, InputAction.GRAB, 0, InputHandler.previousInput[playerEnt.playerIndex, InputAction.ActionToIndex(InputAction.GRAB)] != 100);
                    }
                    break;
            }
        }

        void ClearInput(ALDOKEMAOMB player)
        {
            int nr = player.CJFLMDNNMIE;
            for (int i = 0; i < InputAction.nGameActions; i++)
            {
                InputHandler.currentInput[nr, i] = 0;
            }
        }

        Rect _partnerRect = new Rect(Screen.width - 480, 0, 480, 80);

        void OnGUI()
        {
            if (UIScreen.loadingScreenActive) return; //If game is loading don't show GUI elements
            ConvertTo.ResolutionScale();
            if (DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.ABGFICGIPAD)
            {
                /// _partnerRect = GUI.Window(200, _partnerRect, partnerSelect, "", ATStyle.windStyle);
            }

            if (World.instance != null)
            {
                Cursor.visible = true;

                if (PartnerEntity != null)
                {
                    TrainingPartGUI();
                }

            }
        }

        void PartnerUpdate()
        {
            AudioHandler.PlaySfx(Sfx.MENU_SCROLL);
            AudioHandler.PlayAnnounce(Partner.DOFCCEDJODB);
            Partner.AIINAIDBHJI = (Player.AIINAIDBHJI == CharacterVariant.DEFAULT && Player.LALEEFJMMLH == Partner.LALEEFJMMLH && Partner.DOFCCEDJODB != Character.RANDOM && Partner.DOFCCEDJODB != Character.NONE) ? CharacterVariant.ALT0 : CharacterVariant.DEFAULT;
            LobbySingle.BDMIDGAHNLA(Partner, true);
        }

        public void PartnerSelect(int wId)
        {
            GUILayout.BeginHorizontal(ATStyle.AreaStyle);
            if (GUILayout.Button("◀", ATStyle.Button))
            {
                OnChangeCharacter(false);
            }
            GUILayout.Label($"<b>{JPLELOFJOOH.OAGHLPGCAOI(Partner.LALEEFJMMLH)}</b>", ATStyle.PartnerSelectHeader);
            if (GUILayout.Button("▶", ATStyle.Button))
            {
                OnChangeCharacter(true);
            }
            GUILayout.EndHorizontal();
        }

        void OnChangeCharacter(bool next)
        {
            int i = 0;
            if (next)
            {
                i = characters.FindIndex(x => x == Partner.DOFCCEDJODB) + 1;
                if (i > characters.Count - 1) i = 0;
            }
            else
            {
                i = characters.FindIndex(x => x == Partner.DOFCCEDJODB) - 1;
                if (i < 0) i = characters.Count - 1;
            }

            Partner.DOFCCEDJODB = characters[i];
            PartnerUpdate();
        }

        readonly bool[] trainPartBtn = new bool[2];
#if DEBUG
        public int[,] aTcurInput = new int[4, InputAction.nGameActions];
        public List<int[,]> currentRecordedIns = new List<int[,]>();
        public int frameIncrment = 0;

        bool startRecord;
        bool playReplay;
#endif
        bool refreshTraining;

        bool CharacterTab;
        bool angleExcludeTab;
        int angleExcludePrev;
        bool isAiPartner;
        int characterChangeId;
        int aiLevel;
        bool DownAir => Partner.JCCIAMJEODH.GetCurrentAbility() != null && Partner.JCCIAMJEODH.GetCurrentAbility() == Partner.JCCIAMJEODH.downAirSwingAbility;
        bool NotDefault => characterChangeId != intToCharacter[Partner.LALEEFJMMLH] || aiLevel != (Partner.ALBOPCLADGN ? Partner.INPJBIFEPMB : 0) || isAiPartner != Partner.ALBOPCLADGN;

        readonly List<Character> characters = new List<Character>
        {
            Character.GRAF,
            Character.ELECTRO,
            Character.PONG,
            Character.CROC,
            Character.BOOM,
            Character.ROBOT,
            Character.RANDOM,
            Character.KID,
            Character.CANDY,
            Character.SKATE,
            Character.BOSS,
            Character.COP,
            Character.BAG
        };

        readonly Dictionary<Character, int> intToCharacter = new Dictionary<Character, int> {
            { Character.GRAF, 0},
            {Character.ELECTRO, 1},
            {Character.PONG, 2 },
            {Character.CROC, 3 },
            {Character.BOOM,4 },
            {Character.ROBOT, 5 },
            {Character.KID,6 },
            {Character.CANDY,7 },
            {Character.SKATE,8 },
            {Character.BOSS,9 },
            {Character.COP,10 },
            {Character.BAG,11 }
        };

        public enum AngleMode : byte
        {
            NONE,
            RANDOM,
            NO_REPEAT,
            CYCLE,
            UP,
            DOWN,
            FORWARD,
            BACK,
        }

        public enum SwingType : byte
        {
            NONE,
            SWING,
            SMASH,
            SPIKE,
            BUNT,
            GRAB,
        }

        public enum GetUpType : byte
        {
            NONE,
            STAND,
            BLAZE,
            BUNT,
        }
        void TrainingPartGUI()
        {
            guiRect = GUILayout.Window(101, guiRect, TrainPartnerOptions, "", ATStyle.BlueWindowStyle);
            guiRect = ConvertTo.ClampWindowScaled(guiRect);
        }

        Rect guiRect = new Rect((Screen.width - RECT_WIDTH) - 20, (Screen.height - RECT_HEIGHT) - 10, RECT_WIDTH, RECT_HEIGHT);
        const float RECT_WIDTH = 360;
        const float RECT_HEIGHT = 210;

        int buttonSelected = 0;
        int excludedAngleSelected = 0;

        void TrainPartnerOptions(int wId)
        {
            float insideWidth = RECT_WIDTH - (ATStyle.HeaderAreaStyle.margin.left + ATStyle.HeaderAreaStyle.margin.right);
            GUILayout.Label("Training Partner - " + JPLELOFJOOH.OAGHLPGCAOI(Partner.LALEEFJMMLH), ATStyle.HeaderStyle);
            GUILayout.BeginHorizontal(ATStyle.HeaderAreaStyle);

            GUILayout.BeginVertical(GUILayout.Width(insideWidth * 0.30f));
            buttonSelected = GUILayout.SelectionGrid(buttonSelected, selStrings, 1, ATStyle.ButtonThin);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();

            GUILayout.BeginVertical(GUILayout.Width(6));
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();

            GUILayout.BeginVertical(GUILayout.Width(insideWidth * 0.70f));
            TrainingPartnerPanel(buttonSelected);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
            GUI.DragWindow();
        }

        readonly string[] selStrings = new string[]
        {
            "General",
            "Partner",
            "Parry",
            "Settings"
        };

        void PartnerIntangable()
        {
            if (Ball.ballData.lastHitterIndex != PartnerEntity.playerIndex && noHitPartner)
            {
                DebugSettings.instance.dontGetHit = true;
            }
            else if (DebugSettings.instance.dontGetHit)
            {
                DebugSettings.instance.dontGetHit = false;
            }
        }
        void PartnerGravity()
        {
            if (noGravity)
            {
                PartnerEntity.maxGravity = HHBCPNCDNDH.NKKIFJJEPOL(0);
                PartnerEntity.moveableData.velocity.CGJJEHPPOAN = HHBCPNCDNDH.NKKIFJJEPOL(0);
                if (InputHandler.GetInput(Partner, InputAction.DOWN) && disableMove && !DownAir && !Partner.JCCIAMJEODH.IsInHitpause())
                {
                    PartnerEntity.SetPositionY(HHBCPNCDNDH.FCKBPDNEAOG(PartnerEntity.GetPosition().CGJJEHPPOAN, HHBCPNCDNDH.NKKIFJJEPOL(0.01m))); // Sub 0.01
                }
                else if (InputHandler.GetInput(Partner, InputAction.UP) && disableMove && !Partner.JCCIAMJEODH.IsInHitpause())
                {
                    PartnerEntity.SetPositionY(HHBCPNCDNDH.GAFCIOAEGKD(PartnerEntity.GetPosition().CGJJEHPPOAN, HHBCPNCDNDH.NKKIFJJEPOL(0.01m))); // Add 0.01
                }
                if (PartnerEntity.playerData.hitstunState.ToString().Contains("HIT_BY_BALL")) { noGravity = false; }
            }
            else
            {
                PartnerEntity.maxGravity = maxGravDefault;
            }
        }

        void TrainingPartnerPanel(int panelId = 0)
        {
            GUIStyle style2 = new GUIStyle(GUI.skin.label)
            {
                fontSize = 14,
                alignment = TextAnchor.MiddleCenter,
                margin = new RectOffset(0, 0, 3, 3),
            };
            style2.normal.textColor = Color.white;
            switch (panelId)
            {
                case 0:
                    GUILayout.Label($"Angle Mode: <b>{angleModeId}</b>", style2);
                    angleModeId = (AngleMode)(int)Mathf.Round(GUILayout.HorizontalSlider((int)angleModeId, (int)AngleMode.NONE, (int)AngleMode.BACK, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    if (angleModeId == AngleMode.UP && swingTypeId == SwingType.SPIKE) angleModeId = AngleMode.DOWN;
                    if (angleModeId == AngleMode.BACK && swingTypeId == SwingType.SWING) angleModeId = AngleMode.FORWARD;
                    GUILayout.Label($"Swing Type: <b>{swingTypeId}</b>", style2);
                    swingTypeId = (SwingType)(int)Mathf.Round(GUILayout.HorizontalSlider((int)swingTypeId, (int)SwingType.NONE, (int)SwingType.GRAB, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    GUILayout.Label($"Get Up Type: <b>{getUpTypeId}</b>", style2);
                    getUpTypeId = (GetUpType)(int)Mathf.Round(GUILayout.HorizontalSlider((int)getUpTypeId, (int)GetUpType.NONE, (int)GetUpType.BUNT, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    break;
                case 1:
                    if (CharacterTab = GUILayout.Toggle(CharacterTab, $"Change Character", ATStyle.ButtonThin))
                    {
                        List<Character> changeChara = characters;
                        changeChara.Remove(Character.RANDOM);
                        GUILayout.Label($"<b>{JPLELOFJOOH.OAGHLPGCAOI(changeChara[characterChangeId])}</b>", style2);
                        characterChangeId = (int)Mathf.Round(GUILayout.HorizontalSlider(characterChangeId, 0, characters.Count - 1, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                        //isAiPartner = GUILayout.Toggle(isAiPartner, $"AI Partner: <b>{TextHandler.Get(isAiPartner ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.buttonThin); //AI partner
                        string txt = aiLevel == 0 ? TextHandler.Get("TAG_OFF") : $"Level {aiLevel}";
                        isAiPartner = aiLevel != 0;
                        GUILayout.Label($"AI : <b>{txt}</b>", style2);
                        aiLevel = (int)Mathf.Round(GUILayout.HorizontalSlider(aiLevel, 0, 7, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                        if (NotDefault)
                        {
                            GUILayout.Space(1);
                            if (refreshTraining = GUILayout.Toggle(refreshTraining, "Restart Training", ATStyle.ButtonThin))
                            {
                                Partner.DOFCCEDJODB = changeChara[characterChangeId];
                                Partner.INPJBIFEPMB = aiLevel;
                                Partner.ALBOPCLADGN = isAiPartner;
                                Partner.AIINAIDBHJI = (Player.AIINAIDBHJI == CharacterVariant.DEFAULT && Player.LALEEFJMMLH == Partner.LALEEFJMMLH && Partner.DOFCCEDJODB != Character.RANDOM && Partner.DOFCCEDJODB != Character.NONE) ? CharacterVariant.ALT0 : CharacterVariant.DEFAULT;
                                DNPFJHMAIBP.HOGJDNCMNFP(JOFJHDJHJGI.LGILIJKMKOD, false); //GameStates.Set(GameState.GAME, false);
                                DNPFJHMAIBP.GKBNNFEAJGO(Msg.START, 0, -1);
                                refreshTraining = false;
                                CharacterTab = false;
                                ClearInput(Partner);
                            }
                        }
                    }
                    else
                    {
                        continousAction = GUILayout.Toggle(continousAction, $"Swing Spammer: <b>{TextHandler.Get(continousAction ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
                        noHitPartner = GUILayout.Toggle(noHitPartner, $"Intangible Partner: <b>{TextHandler.Get(noHitPartner ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
                        disableMove = GUILayout.Toggle(disableMove, $"Move Partner [<b>{AdvancedTraining.Instance.SwitchToOtherPlayer.Value}</b>]", ATStyle.ButtonThin);
                        noGravity = GUILayout.Toggle(noGravity, $"Fixed Air Position: <b>{TextHandler.Get(noGravity ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
                        Player.GDEMBCKIDMA = disableMove ? Controller.none : Controller.all;
                        Partner.GDEMBCKIDMA = !disableMove ? Controller.none : Controller.all;
                        characterChangeId = intToCharacter[Partner.LALEEFJMMLH];
                        aiLevel = Partner.ALBOPCLADGN ? Partner.INPJBIFEPMB : 0;
                        isAiPartner = Partner.ALBOPCLADGN;
                    }
                    break;
                case 2:
                    trainPartBtn[0] = GUILayout.Toggle(trainPartBtn[0], $"Parry Hits: <b>{TextHandler.Get(trainPartBtn[0] ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
                    GUILayout.Label($"Parry Chance: <b>{parryChance * 10}%</b>", style2);
                    parryChance = (int)Mathf.Round(GUILayout.HorizontalSlider(parryChance, 1, 10, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    GUILayout.Label($"Parry <b>{parryFrame}</b> Frames Before End: ", style2);
                    parryFrame = (int)Mathf.Round(GUILayout.HorizontalSlider(parryFrame, Mathf.CeilToInt(ConvertTo.Time60Frames(PartnerEntity.startParryBeforeEndMinusThis)), 24, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    break;
                case 3:
                    if (angleExcludeTab = GUILayout.Toggle(angleExcludeTab, $"Remove Angle from Random", ATStyle.ButtonThin))
                    {
                        if (excludedAngleSelected != angleExcludePrev)
                        {
                            angleExcludeTab = false;
                            buttonSelected = 0;
                        }
                        excludedAngleSelected = GUILayout.SelectionGrid(excludedAngleSelected, swingTypeId == SwingType.SPIKE ? excludeDairAngle : excludeAngle, 1, ATStyle.ButtonThinActive);
                    }
                    else
                    {
                        angleExcludePrev = excludedAngleSelected;
                        DebugSettings.instance.dontLoseHP = GUILayout.Toggle(DebugSettings.instance.dontLoseHP, $"Don't Lose HP: <b>{TextHandler.Get(DebugSettings.instance.dontLoseHP ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
                        //DebugSettings.instance.noEnergyRequiredForSpecial = GUILayout.Toggle(DebugSettings.instance.noEnergyRequiredForSpecial, $"No Energy Required: <b>{TextHandler.Get(DebugSettings.instance.noEnergyRequiredForSpecial ? "TAG_ON" : "TAG_OFF")}</b>", ATStyle.ButtonThin);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
