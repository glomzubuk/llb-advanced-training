﻿using GameplayEntities;
using LLHandlers;
using LLScreen;
using UnityEngine;

namespace AdvancedTraining
{
    class StateSystem : MonoBehaviour
    {
        readonly AdvancedTraining AT = AdvancedTraining.Instance;

        PlayerEntity playerEntity;
        ALDOKEMAOMB player;
        Rewired.Controller controller;
        bool modifierKey;

        float gameResetTimer;
        float gameResetDuration = 1f;

        void Start()
        {
            player = ALDOKEMAOMB.BJDPHEHJJJK(0);
            playerEntity = player.JCCIAMJEODH;
            if (AdvancedTraining.IsOnline) { Application.Quit(); }
        }

        bool IsBallSpawning()
        {
            BallEntity ball = BallHandler.instance.GetBall(0);
            return ball.ballData.ballState == BallState.STANDBY || ball.ballData.ballState == BallState.APPEAR;

        }

        void CheckController()
        {
            controller = Rewired.ReInput.controllers.GetLastActiveController();
            if (controller.ImplementsTemplate<Rewired.IGamepadTemplate>())
            {
                Rewired.IGamepadTemplate gamepad = controller.GetTemplate<Rewired.IGamepadTemplate>();
                modifierKey = gamepad.center1.value;
            }
        }

        private void Update()
        {
            if (World.instance != null)
            {
                if (AdvancedTraining.IsOnline && AdvancedTraining.CurrentGameMode != GameMode.TRAINING)
                {
                    AdvancedTraining.Log.LogError("\"StateSystem\" detected online Terminationg LLB");
                    AudioHandler.PlaySfx(Sfx.KILL);
                    Application.Quit();
                    return;
                }
                CheckController();

                if (CountDown(ref gameResetTimer, gameResetDuration) && (Input.GetKeyDown(AT.QuickRestart.Value) || modifierKey && InputHandler.GetInput(ALDOKEMAOMB.BJDPHEHJJJK(0), InputAction.TAUNT)) && playerEntity.playerData.playerState != PlayerState.SPECIAL)
                {
                    gameResetTimer += Time.deltaTime;
                    QuickRestart();
                }
                FrameManipulation();
            }
        }

        bool CountDown(ref float timer, float duration)
        {
            if (timer > 0 && timer < duration) // Cooldown in seconds
            {
                timer += Time.deltaTime;
            }
            else
            {
                timer = 0;
            }
            return timer == 0;
        }

        void QuickRestart()
        {
            OGONAGCFDPK.instance.ENNNJNLANCO(true, 0); // SpawnPlayers : ForceRespawn = true, onlyPlayer: 0
            OGONAGCFDPK.instance.ENNNJNLANCO(true, 1); // SpawnPlayers : ForceRespawn = true, onlyPlayer: 1
            OGONAGCFDPK.instance.KIAKGLIBMCG(); // Despawn Ball
            PlayerHandler.instance.playerHandlerData.firstLosingPlayerOfBurst = ALDOKEMAOMB.BJDPHEHJJJK(0).JCCIAMJEODH.playerIndex; // Gets the PlayerEntity of the Main controlling player
            BallHandler.instance.SpawnBall(0);
            StageBackground.BG.SetState(StageBackground.BGState.NORMAL, true);
            ScreenGameHud.UpdateSpeed(BallHandler.instance.GetBall(0).ballData.flySpeed);
            AudioHandler.PlaySfx(Sfx.MENU_CONFIRM);
        }

        enum AS
        {
            INACTIVE,
            ACTIVE,
            PAUSED
        }

        float frameSkipTimer;
        float frameSkipBufferTimer;
        const float frameSkipperAfterTime = 0.5f;

        bool FrameSkipper()
        {
            frameSkipBufferTimer += Time.deltaTime;
            return frameSkipBufferTimer > frameSkipperAfterTime;
        }

        void PauseTime()
        {
            if (World.state != (NMNBAFMOBNA)AS.PAUSED) World.state = (NMNBAFMOBNA)AS.PAUSED;
            else World.state = (NMNBAFMOBNA)AS.ACTIVE;
            StageBackground.BG.timeScale = (World.state == (NMNBAFMOBNA)AS.ACTIVE) ? 1f : 0f;
            AudioHandler.DuckMusic(World.state == (NMNBAFMOBNA)AS.PAUSED);
        }
        static bool InPauseMenu()
        {
            foreach (var screen in UIScreen.currentScreens)
            {
                if (screen == null) { continue; }
                if (screen.screenType == ScreenType.GAME_PAUSE)
                {
                    return true;
                }
            }
            return false;
        }

        void FrameManipulation()
        {
            if (AdvancedTraining.InGame && AdvancedTraining.IsOnline == false)
            {
                World world = World.instance;

                // If the KO Cam State is in "STANDBY". On Press the pauseTimeKey will freeze on the current frame and lower the volume. Releasing the key will unfreeze the frame | Default: R key
                if (world.worldData.koCamState == AEJIEDFMDJM.BHAIFNKJPAF || world.worldData.koCamState == AEJIEDFMDJM.GAOABBKLMDF)
                {
                    if (Input.GetKeyDown(AT.PauseTimeKey.Value))
                    {
                        PauseTime();
                    }
                }

                if (!InPauseMenu())
                {
                    // Advances to the next frame, but only when the game world is in a paused state | Default: T key
                    if ((Input.GetKeyDown(AT.AdvanceFrame.Value) || AT.ModifierKeyDownAct()) && World.state == NMNBAFMOBNA.CBPEHBCCEMG)
                    {
                        world.play_One_Frame = true;
                    }

                    //Frame Advance Buffer - Continous Frame Advance
                    if ((Input.GetKey(AT.AdvanceFrame.Value) || AT.ModifierKeyAct()) && World.state == NMNBAFMOBNA.CBPEHBCCEMG)
                    {
                        frameSkipTimer += Time.deltaTime;
                        if (FrameSkipper() && frameSkipTimer > 0.125f) // Cooldown in seconds
                        {
                            world.play_One_Frame = true;
                            frameSkipTimer = 0;
                        }
                    }
                    else
                    {
                        frameSkipTimer = 0;
                        frameSkipBufferTimer = 0;
                    }
                }

                // Save's the current frame | Default: F2 key
                if (Input.GetKeyDown(AT.SaveStateKey.Value))
                {
                    world.SaveState();
                }

                // Load's the saved frame | Default: F3 key
                if (Input.GetKeyDown(AT.LoadStateKey.Value))
                {
                    world.LoadState();
                }

                if (!IsBallSpawning())
                {
                    if (AT.ModifierKeyAct() && InputHandler.GetInput(ALDOKEMAOMB.BJDPHEHJJJK(0), InputAction.EXPRESS_DOWN) && playerEntity.playerData.playerState != PlayerState.SPECIAL)
                    {
                        BallEntity ball = BallHandler.instance.GetBall(0);
                        ball.ballData.lastHitterIndex = 0;
                        ball.SetToTeam(BGHNEHPFHGC.EHPJJADIPNG);
                        ball.SetBallState(BallState.STICK_TO_PLAYER_SERVE);
                        playerEntity.EndAbilityStateToNormal();
                    }
                }

#if DEBUG
                if (Input.GetKeyDown(KeyCode.U))
                {
                    AT.SpawnTarget();
                }

                if (Input.GetKeyDown(KeyCode.I))
                {
                    foreach (TargetEntity targetEntity in FindObjectsOfType<TargetEntity>())
                    {
                        targetEntity.outtaHere = true;
                        if (targetEntity.outtaHere)
                        {
                            targetEntity.entityData.active = false;
                            targetEntity.SetPosition(World.OFF_SCREEN_POSITION);
                            targetEntity.UpdateUnityTransform();
                        }
                    }
                }
                if (AT.targets != null)
                {
                    foreach (TargetEntity targetEntity in AT.targets)
                    {
                        if (BallHandler.instance.GetBall(0).ballData.isDirect)
                        {
                            targetEntity.CheckHit(BallHandler.instance.GetBall(0).GetPosition(), BallHandler.instance.GetBall(0).ballData.team);
                        }
                    }
                }
#endif
            }
        }
    }
}
