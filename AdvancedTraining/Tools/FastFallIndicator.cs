﻿using GameplayEntities;
using LLHandlers;
using UnityEngine;

namespace AdvancedTraining
{
    class FastFallIndicator : MonoBehaviour
    {
        private PlayerEntity mainPlayerEntity = null;
        private

        void Start()
        {
            mainPlayerEntity = PlayerHandler.instance.GetPlayerEntity(0);
            AudioHandler.PlaySfx(Sfx.MENU_CONFIRM);
        }

        void OnDestroy()
        {
            mainPlayerEntity.SetColorOutlines(false);
            mainPlayerEntity.GetVisual().ResetMaterials();
            AudioHandler.PlaySfx(Sfx.MENU_SET);
        }

        void Update()
        {
            if (mainPlayerEntity.moveableData.velocity.CGJJEHPPOAN.EPOACNMBMMN < 0)
            {
                mainPlayerEntity.SetColorOutlines(true);
                if (mainPlayerEntity.moveableData.velocity.CGJJEHPPOAN.EPOACNMBMMN <= -mainPlayerEntity.maxGravity.EPOACNMBMMN)
                {
                    mainPlayerEntity.SetColorOutlinesColor(BGHNEHPFHGC.JAGOLMCFMHD); // Yellow Team
                }
                else
                {
                    mainPlayerEntity.SetColorOutlinesColor(BGHNEHPFHGC.PDOIGCCJJEO); // Blue Team
                }
            }
            else
            {
                mainPlayerEntity.SetColorOutlines(false);
            }

#if DEBUG
            RecoveryIndicator();
#endif
        }

#if DEBUG

        ///<summary> Is an indicator that shows when you can't move and when you are in an action.</summary>
        void RecoveryIndicator()
        {
            if ((mainPlayerEntity.moveableData.playerState == PlayerState.ACTION || mainPlayerEntity.moveableData.playerState == PlayerState.HITPAUSE || mainPlayerEntity.moveableData.playerState == PlayerState.HITSTUN) && !(mainPlayerEntity.abilityData.abilityState.Contains("START_RUN") || mainPlayerEntity.abilityData.abilityState.Contains("CROUCH") && mainPlayerEntity.abilityData.bufferAbility == ""))
            {
                mainPlayerEntity.GetVisual("main").SetMainColor(Color.blue);
                if (mainPlayerEntity.abilityData.abilityState.Contains("CHARGE") && HHBCPNCDNDH.OAHDEOGKOIM(mainPlayerEntity.abilityData.abilityStateTimer, ConvertTo.FramesDuration60fps(3)) && HHBCPNCDNDH.HGDAIHMEFKC(HHBCPNCDNDH.AJOCFFLIIIH(mainPlayerEntity.abilityData.abilityStateTimer, HHBCPNCDNDH.NKKIFJJEPOL(20))) % 2 != 0)
                {
                    mainPlayerEntity.GetVisual("main").SetMainColor(new Color(0.8f, 0.8f, 0.8f));
                }
            }
            else
            {
                mainPlayerEntity.GetVisual("main").ResetMaterials();
            }
        }
#endif
    }
}
