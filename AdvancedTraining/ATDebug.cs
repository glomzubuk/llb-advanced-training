﻿using Abilities;
using GameplayEntities;
using LLHandlers;
using LLScreen;
using System.Collections.Generic;
using UnityEngine;

namespace AdvancedTraining
{
#if DEBUG
    class ATDebug : MonoBehaviour
    {




        Rect windowRect = new Rect(Screen.width / 2 - 150, Screen.height / 2 - 500, 300, 1200);
        Rect ballInfoRect = new Rect(Screen.width / 2 - 150, Screen.height / 2 - 500, 300, 1000);
        Rect inputRect = new Rect(0, Screen.height - 160, 550, 160);
        Rect inputRect2 = new Rect(Screen.width - 550, Screen.height - 160, 550, 160);
        Rect player1Rect = new Rect(0, 0, 300, 460);
        Rect player2Rect = new Rect(300, 0, 300, 460);
        Rect player3Rect = new Rect(600, 0, 300, 460);
        Rect player4Rect = new Rect(900, 0, 300, 460);
        Rect debugRect = new Rect(Screen.width / 2 - 150, Screen.height / 2 - 200, 300, 200);
        GUIStyle bgStyle = new GUIStyle();
        GUIStyle bgStyle2 = new GUIStyle();
        GUIStyle bgStyle3 = new GUIStyle();
        GUIStyle bgStyle4 = new GUIStyle();

        bool showBallData;
        bool showPlayerData;
        bool showInputData;


        List<Texture2D> colours = new List<Texture2D>();

        void Start()
        {
            DebugSettings.instance.createFrameLogs = true;
            colours.Add(ColorToTexture2D(JPLELOFJOOH.EGJAKCLDCGB((BGHNEHPFHGC)0)));
            colours.Add(ColorToTexture2D(JPLELOFJOOH.EGJAKCLDCGB((BGHNEHPFHGC)1)));
            colours.Add(ColorToTexture2D(JPLELOFJOOH.EGJAKCLDCGB((BGHNEHPFHGC)2)));
            colours.Add(ColorToTexture2D(JPLELOFJOOH.EGJAKCLDCGB((BGHNEHPFHGC)3)));
            colours.Add(ColorToTexture2D(Color.black));
            debugScroll = new Vector2[4];
        }

        void OnDestroy()
        {
            DebugSettings.instance.createFrameLogs = false;
        }

        float actionCounter = 0;
        bool actionContinu;
        bool turnNow;
        byte actionIndex = 0;
        List<string> actionList = new List<string> { "crouch", "neutralSwing", "bunt", "grab", "smash", "downAirSwing", "wallSwing", "knockedOut" };

        const byte numberOfAct = 2;


        void Update()
        {

            ALDOKEMAOMB player = ALDOKEMAOMB.BJDPHEHJJJK(0);
            PlayerEntity playerEntity = player.JCCIAMJEODH;

            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                playerEntity.SetPlayerState(PlayerState.SPECIAL, "ROBOT_KICKFLIP", HitPauseState.NONE, HitstunState.NONE);
                playerEntity.entityData.velocity = ConvertTo.Vector2f(Vector2.zero);
            }

            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                playerEntity.SetPlayerState(PlayerState.SPECIAL, "ROBOT_CEILING_KICKFLIP", HitPauseState.NONE, HitstunState.NONE);
                playerEntity.entityData.velocity = ConvertTo.Vector2f(Vector2.zero);
            }

            if (Input.GetKeyDown(KeyCode.End))
            {

                Camera component = GameCamera.instance.transform.GetChild(2).GetComponent<Camera>();
                component.cullingMask = (component.cullingMask == 0) ? 114689 : 0;//((!enable) ? 0 : 114689);
                component.clearFlags = CameraClearFlags.Color;
                component.backgroundColor = Color.green;
                ALDOKEMAOMB.BJDPHEHJJJK(0).JCCIAMJEODH.SetPositionX(new HHBCPNCDNDH(-4));
                playerEntity.moveableData.velocity = ConvertTo.Vector2f(Vector2.zero);
                foreach (var item in playerEntity.gameObject.GetComponent<VisualEntity>().skinRenderers)
                {
                    item.shadowCastingMode = component.cullingMask == 0 ? UnityEngine.Rendering.ShadowCastingMode.Off : UnityEngine.Rendering.ShadowCastingMode.On;
                }

                //BallHandler.instance.GetBall().SetPositionX(new HHBCPNCDNDH(-4));

                playerEntity.GetVisual("main").rendererOutline.shadowCastingMode = component.cullingMask == 0 ? UnityEngine.Rendering.ShadowCastingMode.Off : UnityEngine.Rendering.ShadowCastingMode.On;
            }

            if (Input.GetKeyDown(KeyCode.Home) || actionContinu == true)
            {

                if (actionContinu == false)
                {
                    playerEntity.ResetAnimation();
                    DebugSettings.instance.testAirAttacksOnGround = true;
                }

                if (playerEntity.GetAnimDataOfVisual("main").currentAnim == "idle" && turnNow == true)
                {
                    playerEntity.moveableData.heading = (playerEntity.moveableData.heading == Side.LEFT) ? Side.RIGHT : Side.LEFT;
                    turnNow = false;
                }

                if (playerEntity.DoingAbility() == false && playerEntity.GetAnimDataOfVisual("main").currentAnim == "idle" && ConvertTo.Float(playerEntity.GetAnimDataOfVisual("main").animTime) > 0.06f)
                {
                    playerEntity.StartAbility(actionList[actionIndex]);
                    actionCounter++;
                    turnNow = true;
                    if (actionCounter % 2 == 0)
                    {
                        actionIndex++;
                    }
                }

                actionContinu = true;

                if (actionCounter >= actionList.Count * 2 && playerEntity.DoingAbility() == false)
                {
                    actionIndex = 0;
                    actionContinu = false;
                    turnNow = false;
                    actionCounter = 0;
                    DebugSettings.instance.testAirAttacksOnGround = false;
                }
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                float num = 0.01f;
                World.instance.stageMax.CGJJEHPPOAN = Input.GetKey(KeyCode.LeftShift) ? ConvertTo.Add(World.instance.stageMax.CGJJEHPPOAN, ConvertTo.Floatf(num)) : ConvertTo.Subtract(World.instance.stageMax.CGJJEHPPOAN, ConvertTo.Floatf(num));
                World.instance.stageSize.CGJJEHPPOAN = Input.GetKey(KeyCode.LeftShift) ? ConvertTo.Add(World.instance.stageSize.CGJJEHPPOAN, ConvertTo.Floatf(num)) : ConvertTo.Subtract(World.instance.stageSize.CGJJEHPPOAN, ConvertTo.Floatf(num));
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                float num = 0.01f;
                World.instance.stageMax.GCPKPHMKLBN = Input.GetKey(KeyCode.LeftShift) ? ConvertTo.Subtract(World.instance.stageMax.GCPKPHMKLBN, ConvertTo.Floatf(num)) : ConvertTo.Add(World.instance.stageMax.GCPKPHMKLBN, ConvertTo.Floatf(num));
                World.instance.stageMin.GCPKPHMKLBN = Input.GetKey(KeyCode.LeftShift) ? ConvertTo.Add(World.instance.stageMin.GCPKPHMKLBN, ConvertTo.Floatf(num)) : ConvertTo.Subtract(World.instance.stageMin.GCPKPHMKLBN, ConvertTo.Floatf(num));
                World.instance.stageSize.GCPKPHMKLBN = Input.GetKey(KeyCode.LeftShift) ? ConvertTo.Subtract(World.instance.stageSize.GCPKPHMKLBN, ConvertTo.Floatf(num * 2)) : ConvertTo.Add(World.instance.stageSize.GCPKPHMKLBN, ConvertTo.Floatf(num * 2));
            }

            // Hide/Show the Speed Boombox and PlayerInfo
            if (Input.GetKeyDown(KeyCode.F11))
            {
                GameCamera.instance.GetComponent<Camera>().enabled = !GameCamera.instance.GetComponent<Camera>().enabled;
                GameCamera.instance.transform.GetChild(1).GetComponent<Camera>().enabled = !GameCamera.instance.transform.GetChild(1).GetComponent<Camera>().enabled;
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                showInputData = !showInputData;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                showBallData = !showBallData;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                showPlayerData = !showPlayerData;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                showCharacterInfo = !showCharacterInfo;
            }
            if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                IBGCBLLKIHA ibgcbllkiha = playerEntity.GetCurrentlyActiveHurtbox().bounds.IACOKHPMNGN;
                AdvancedTraining.Log.LogDebug(string.Format(string.Concat(new string[]
                {
                        "\n",
                        playerEntity.characterIdentifier.ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.maxMove).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.maxAirMove).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.maxGravity).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.groundAcc).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.groundDeacc).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.airAcc).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.airDeacc).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.jumpPower).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.extraJumpPower).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.gravityForce).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.gravityForceFastFall).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.gravityForceUp).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.gravityForceApex).ToString(),
                        "\n",
                        "N/a",
                        "\n",
                        "N/a",
                        "\n",
                        ConvertTo.Float(playerEntity.highJumpPower).ToString(),
                        "\n",
                        ConvertTo.Float(playerEntity.chargeMaxDuration).ToString(),
                        "\n",
                        "\n",
                        "\n",
                        playerEntity.hitAngleDown.ToString(),
                        "\n",
                        playerEntity.hitAngleDownAirBackward.ToString(),
                        "\n",
                        playerEntity.hitAngleDownAirForward.ToString(),
                        "\n",
                        playerEntity.hitAngleNeutralDownAir.ToString(),
                        "\n",
                        playerEntity.hitAngleSmash.ToString(),
                        "\n",
                        playerEntity.hitAngleUp.ToString(),
                        "\n",
                        "\n",
                        "\n",
                        ConvertTo.Float(ibgcbllkiha.GCPKPHMKLBN).ToString(),
                        "\n",
                        ConvertTo.Float(ibgcbllkiha.CGJJEHPPOAN).ToString(),
                        "\n"
                })));
                AdvancedTraining.Log.LogDebug(string.Format(string.Concat(new string[]
                {
                            playerEntity.characterIdentifier.ToString() + " : maxMove | " + ConvertTo.Float(playerEntity.maxMove).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : maxAirMove | " + ConvertTo.Float(playerEntity.maxAirMove).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : maxGravity | " + ConvertTo.Float(playerEntity.maxGravity).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : groundAcc | " + ConvertTo.Float(playerEntity.groundAcc).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : groundDeacc | " + ConvertTo.Float(playerEntity.groundDeacc).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : airAcc | " + ConvertTo.Float(playerEntity.airAcc).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : airDeacc | " + ConvertTo.Float(playerEntity.airDeacc).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : jumpPower | " + ConvertTo.Float(playerEntity.jumpPower).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : extraJumpPower | " + ConvertTo.Float(playerEntity.extraJumpPower).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : gravityForce | " + ConvertTo.Float(playerEntity.gravityForce).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : gravityForceFastFall | " + ConvertTo.Float(playerEntity.gravityForceFastFall).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : gravityForceUp | " + ConvertTo.Float(playerEntity.gravityForceUp).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : gravityForceApex | " + ConvertTo.Float(playerEntity.gravityForceApex).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : apexIn | " + ConvertTo.Float(playerEntity.apexIn).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : apexOut | " + ConvertTo.Float(playerEntity.apexOut).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : highJumpPower | " + ConvertTo.Float(playerEntity.highJumpPower).ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : hitAngleDown | " + playerEntity.hitAngleDown.ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : hitAngleDownAirBackward | " + playerEntity.hitAngleDownAirBackward.ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : hitAngleDownAirForward | " + playerEntity.hitAngleDownAirForward.ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : hitAngleNeutralDownAir | " + playerEntity.hitAngleNeutralDownAir.ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : hitAngleSmash | " + playerEntity.hitAngleSmash.ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : hitAngleUp | " + playerEntity.hitAngleUp.ToString() + "\n",
                            playerEntity.characterIdentifier.ToString() + " : throwOffset | " + ConvertTo.Vector2(playerEntity.throwOffset).ToString() + "\n",
                }), new object[0]));

                if (Input.GetKeyDown(KeyCode.KeypadEnter))
                {
                    for (int i = 0; i < World.instance.playerHandler.PlayersInPlay(); i++)
                    {
                        PlayerEntity playerEntity2 = PlayerHandler.instance.GetPlayerEntity(i);
                        string[] array = new string[playerEntity2.hitboxes.Count];
                        string[] array2 = new string[playerEntity2.hurtboxes.Count];
                        List<string> list = new List<string>();
                        List<string> list2 = new List<string>();
                        int num = 0;
                        foreach (KeyValuePair<string, Ability> keyValuePair in playerEntity2.abilities)
                        {
                            list.Add("\n\n" + keyValuePair.Key);
                            foreach (string name in playerEntity2.GetAbility(keyValuePair.Key).abilityStatesSequence)
                            {
                                list.Add(string.Concat(new string[]
                                {
                                    "\n",
                                    playerEntity2.characterIdentifier,
                                    " : ",
                                    playerEntity2.GetAbilityState(name).name.ToString(),
                                    " | ",
                                    ConvertTo.Time60Frames(playerEntity2.GetAbilityState(name).duration).ToString()
                                }));
                            }
                        }
                        foreach (KeyValuePair<string, AbilityState> keyValuePair2 in playerEntity2.abilityStates)
                        {
                            if (ConvertTo.Float(playerEntity2.GetAbilityState(keyValuePair2.Key).duration) != 0f)
                            {
                                list2.Add(string.Concat(new string[]
                                {
                                    "\n",
                                    playerEntity2.characterIdentifier,
                                    " : ",
                                    playerEntity2.GetAbilityState(keyValuePair2.Key).name.ToString(),
                                    " | ",
                                    ConvertTo.Time60Frames(playerEntity2.GetAbilityState(keyValuePair2.Key).duration).ToString()
                                }));
                                foreach (string item in list)
                                {
                                    if (list2.Contains(item))
                                    {
                                        list2.Remove(item);
                                    }
                                }
                            }
                        }
                        foreach (KeyValuePair<string, PlayerHitbox> keyValuePair3 in playerEntity2.hitboxes)
                        {
                            array[num] = string.Concat(new string[]
                            {
                                "\n",
                                playerEntity2.characterIdentifier,
                                " : ",
                                keyValuePair3.Key.ToString(),
                                " | ",
                                keyValuePair3.Value.ToString()
                            });
                            num++;
                        }
                        num = 0;
                        foreach (KeyValuePair<string, PlayerBox> keyValuePair4 in playerEntity2.hurtboxes)
                        {
                            array2[num] = string.Concat(new string[]
                            {
                                "\n",
                                playerEntity2.characterIdentifier,
                                " : ",
                                keyValuePair4.Key.ToString(),
                                " | ",
                                keyValuePair4.Value.ToString()
                            });
                            num++;
                        }
                        num = 0;
                        list.ToArray();
                        string[] values = list.ToArray();
                        string[] values2 = list2.ToArray();
                        AdvancedTraining.Log.LogDebug(string.Format(string.Concat(new string[]
                        {
                            playerEntity2.characterIdentifier.ToString() + " : maxMove | " + ConvertTo.Float(playerEntity2.maxMove).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : maxAirMove | " + ConvertTo.Float(playerEntity2.maxAirMove).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : maxGravity | " + ConvertTo.Float(playerEntity2.maxGravity).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : groundAcc | " + ConvertTo.Float(playerEntity2.groundAcc).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : groundDeacc | " + ConvertTo.Float(playerEntity2.groundDeacc).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : airAcc | " + ConvertTo.Float(playerEntity2.airAcc).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : airDeacc | " + ConvertTo.Float(playerEntity2.airDeacc).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : jumpPower | " + ConvertTo.Float(playerEntity2.jumpPower).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : extraJumpPower | " + ConvertTo.Float(playerEntity2.extraJumpPower).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : gravityForce | " + ConvertTo.Float(playerEntity2.gravityForce).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : gravityForceFastFall | " + ConvertTo.Float(playerEntity2.gravityForceFastFall).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : gravityForceUp | " + ConvertTo.Float(playerEntity2.gravityForceUp).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : gravityForceApex | " + ConvertTo.Float(playerEntity2.gravityForceApex).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : apexIn | " + ConvertTo.Float(playerEntity2.apexIn).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : apexOut | " + ConvertTo.Float(playerEntity2.apexOut).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : highJumpPower | " + ConvertTo.Float(playerEntity2.highJumpPower).ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : hitAngleDown | " + playerEntity2.hitAngleDown.ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : hitAngleDownAirBackward | " + playerEntity2.hitAngleDownAirBackward.ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : hitAngleDownAirForward | " + playerEntity2.hitAngleDownAirForward.ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : hitAngleNeutralDownAir | " + playerEntity2.hitAngleNeutralDownAir.ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : hitAngleSmash | " + playerEntity2.hitAngleSmash.ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : hitAngleUp | " + playerEntity2.hitAngleUp.ToString() + "\n",
                            playerEntity2.characterIdentifier.ToString() + " : throwOffset | " + ConvertTo.Vector2(playerEntity2.throwOffset).ToString() + "\n",
                            string.Concat(array),
                            string.Concat(array2),
                            "\n\nAbilities",
                            string.Concat(values),
                            "\n\nAbilitie States",
                            string.Concat(values2)
                        }), new object[0]));
                    }

                }
            }
        }

        private void OnGUI()
        {

            Vector2 conOffset = new Vector2(0, 3);
            bgStyle.padding = new RectOffset(0, 0, 6, 0);
            bgStyle.normal.background = colours[0];
            bgStyle2.normal.background = colours[1];
            bgStyle3.normal.background = colours[2];
            bgStyle4.normal.background = colours[3];
            bgStyle.alignment = TextAnchor.UpperCenter;
            bgStyle2.alignment = TextAnchor.UpperCenter;
            bgStyle3.alignment = TextAnchor.UpperCenter;
            bgStyle4.alignment = TextAnchor.UpperCenter;
            bgStyle.contentOffset = conOffset;
            bgStyle2.contentOffset = conOffset;
            bgStyle3.contentOffset = conOffset;
            bgStyle4.contentOffset = conOffset;

            GUIStyle labStyle = new GUIStyle(GUI.skin.label)
            {
                fontSize = 12,
            };

            GUI.Label(new Rect(Screen.width - 150, Screen.height - 40, 140, 30), "DEBUG MODE ");

            if (showPlayerData)
            {
                player1Rect = GUI.Window(0, player1Rect, DebugWindow, ALDOKEMAOMB.BJDPHEHJJJK(0).COPBANHBAEE, bgStyle);
                if (ALDOKEMAOMB.BJDPHEHJJJK(1).NGLDMOLLPLK || true)
                {
                    player2Rect = GUI.Window(1, player2Rect, DebugWindow, ALDOKEMAOMB.BJDPHEHJJJK(1).COPBANHBAEE, bgStyle2);
                }
                if (ALDOKEMAOMB.BJDPHEHJJJK(2).NGLDMOLLPLK)
                {
                    player3Rect = GUI.Window(2, player3Rect, DebugWindow, ALDOKEMAOMB.BJDPHEHJJJK(2).COPBANHBAEE, bgStyle3);
                }
                if (ALDOKEMAOMB.BJDPHEHJJJK(3).NGLDMOLLPLK)
                {
                    player4Rect = GUI.Window(3, player4Rect, DebugWindow, ALDOKEMAOMB.BJDPHEHJJJK(3).COPBANHBAEE, bgStyle4);
                }
            }


            if (showPlayerData)
            {
                //windowRect = GUI.Window(4, windowRect, InfoWindow, "Info Menu", ATStyle.windStyle);
            }

            if (showBallData)
            {
                ballInfoRect = GUI.Window(5, ballInfoRect, BallInfoMenu, "Ball Info Menu", ATStyle.WindStyle);
            }

            if (showInputData)
            {
                inputRect = GUI.Window(6, inputRect, InputWindow, "Input Data: " + ALDOKEMAOMB.BJDPHEHJJJK(0).COPBANHBAEE, ATStyle.WindStyle);
                inputRect2 = GUI.Window(7, inputRect2, InputWindow, "Input Data: " + ALDOKEMAOMB.BJDPHEHJJJK(1).COPBANHBAEE, ATStyle.WindStyle);
            }

            if (showCharacterInfo)
            {
                charaRect = GUI.Window(8, charaRect, charaInfo, "Info Window");
            }
            //debugRect = GUI.Window(124, debugRect, DebugMenu, "Debug Menu", bgStyle);

        }

        private void DebugWindow(int wId)
        {
            BallEntity ball = BallHandler.instance.GetBall();
            ALDOKEMAOMB player = ALDOKEMAOMB.BJDPHEHJJJK(wId);
            List<string> playerDataList = new List<string>();
            string txt = "[{0}]: {1} : {2}";
            int num = 1;
            var prop11 = string.Format(txt, num++, "LEFT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.LEFT)] + " : " + InputAction.ActionToIndex(InputAction.LEFT)); // joinedMatch
            var prop12 = string.Format(txt, num++, "RIGHT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.RIGHT)] + " : " + InputAction.ActionToIndex(InputAction.RIGHT)); // joinedMatch
            var prop13 = string.Format(txt, num++, "UP", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.UP)] + " : " + InputAction.ActionToIndex(InputAction.UP)); // joinedMatch
            var prop14 = string.Format(txt, num++, "DOWN", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.DOWN)] + " : " + InputAction.ActionToIndex(InputAction.DOWN)); // joinedMatch
            var prop15 = string.Format(txt, num++, "SWING", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.SWING)] + " : " + InputAction.ActionToIndex(InputAction.SWING)); // joinedMatch

            num = 1;
            playerDataList.Add(string.Format(txt, num++, "Player Index", player.CJFLMDNNMIE));
            playerDataList.Add(string.Format(txt, num++, "Name", player.COPBANHBAEE));
            playerDataList.Add(string.Format(txt, num++, "Controller", player.GDEMBCKIDMA));
            playerDataList.Add(string.Format(txt, num++, "Player Entity", player.JCCIAMJEODH));
            playerDataList.Add(string.Format(txt, num++, "Is Local?", player.GAFCIHKIGNM));
            playerDataList.Add(string.Format(txt, num++, "IP", player.LGKJGGMLNFL));
            playerDataList.Add(string.Format(txt, num++, "Port", player.MIHNOINHDJN));
            playerDataList.Add(string.Format(txt, num++, "CPU Selecting", player.OLNANNOFOJO));
            playerDataList.Add(string.Format(txt, num++, "Ready?", player.GFCMODHPPCN));
            playerDataList.Add(string.Format(txt, num++, "Selected?", player.CHNGAKOIJFE));
            playerDataList.Add(string.Format(txt, num++, "Ai Level", player.INPJBIFEPMB));
            playerDataList.Add(string.Format(txt, num++, "Character", player.LALEEFJMMLH));
            playerDataList.Add(string.Format(txt, num++, "Character Variant", player.AIINAIDBHJI));
            playerDataList.Add(string.Format(txt, num++, "Character Selected", player.DOFCCEDJODB));
            playerDataList.Add(string.Format(txt, num++, "Local Peer", player.MIBJCHAIFDM));
            playerDataList.Add(string.Format(txt, num++, "Joined Match", player.PNHOIDECPJE));
            playerDataList.Add(string.Format(txt, num++, "In Match", player.NGLDMOLLPLK));
            playerDataList.Add(string.Format(txt, num++, "Disconnected", player.CMCIGIEHGHO));
            playerDataList.Add(string.Format(txt, num++, "is Ai?", player.ALBOPCLADGN));
            playerDataList.Add(string.Format(txt, num++, "is Spectator?", player.CHGMPLJHKFN));
            playerDataList.Add(string.Format(txt, num++, "character Selected Random", player.IMJLOPPPIJM));
            //playerDataList.Add(string.Format(txt, num++, "nPlayersJoinedMatch", player.nPlayersJoinedMatch));
            //playerDataList.Add(string.Format(txt, num++, "nPlayersInMatch", player.nPlayersInMatch));
            playerDataList.Add(string.Format(txt, num++, "Cursor", player.OBELDJGOOIJ));
            playerDataList.Add(string.Format(txt, num++, "is Active?", player.OBELDJGOOIJ.GetActive()));
            playerDataList.Add(string.Format(txt, num++, "Position", player.OBELDJGOOIJ.GetPosition()));
            playerDataList.Add(string.Format(txt, num++, "State", player.OBELDJGOOIJ.GetState()));

            GUILayout.BeginArea(new Rect(0, 0, player1Rect.width, player1Rect.height), ATStyle.border);
            debugScroll[wId] = GUILayout.BeginScrollView(debugScroll[wId], ATStyle.MainStyle);
            foreach (string name in playerDataList)
            {
                GUILayout.Label(name, ATStyle.LabStyleLeft);
            }
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            GUI.DragWindow();

        }

        Vector2 scrollView;

        private void InfoWindow(int wId)
        {
            ALDOKEMAOMB player = ALDOKEMAOMB.BJDPHEHJJJK(0);
            PlayerEntity playerEntity = player.JCCIAMJEODH;
            AbilityState aState = playerEntity.GetCurrentAbilityState();
            Ability ability = playerEntity.GetCurrentAbility();

            string txt = "[{0}]: {1}";
            var str1 = string.Format(txt, "abilityState", playerEntity.playerData.abilityState);
            var str2 = string.Format(txt, "abilityStateTimer", ConvertTo.Divide(playerEntity.playerData.abilityStateTimer, World.FDELTA_TIME));
            var str4 = string.Format(txt, "Hitstun", playerEntity.playerData.hitstunState);
            var str5 = string.Format(txt, "playerState", playerEntity.playerData.playerState);
            var str6 = string.Format(txt, "playerState", playerEntity.attackingData.hitPauseState);
            var str7 = string.Format(txt, "chargePower", playerEntity.attackingData.chargePower);
            var str8 = string.Format(txt, "hitSomething", playerEntity.attackingData.hitSomething);
            var str9 = string.Format(txt, "bigHitsCount", playerEntity.abilityData.bigHitsCount);
            var str10 = string.Format(txt, "bigHitsCountAdditional", playerEntity.abilityData.bigHitsCountAdditional);
            var str11 = string.Format(txt, "checkedActionsThisFrame", playerEntity.abilityData.checkedActionsThisFrame);
            var str12 = string.Format(txt, "preHeading", playerEntity.abilityData.preHeading);
            var str13 = string.Format(txt, "specialAmount", playerEntity.abilityData.specialAmount); //Used by sonata's Special and goes down from 2 in unmodified conditions
            var str14 = string.Format(txt, "specialFAmount", playerEntity.abilityData.specialFAmount); //Used by characters who can fly. Jet & Doombox
            var str15 = string.Format(txt, "specialVector", playerEntity.abilityData.specialVector); //Used by sonata's special to determine the balls direction and can be modified with taunting
            var str16 = string.Format(txt, "specialVector2", playerEntity.abilityData.specialVector2);
            var str17 = string.Format(txt, "specialBool", playerEntity.abilityData.specialBool);
            var str18 = string.Format(txt, "eatBallTimer", playerEntity.abilityData.eatBallTimer);
            var str19 = string.Format(txt, "eatBallMoveDuration", playerEntity.abilityData.eatBallMoveDuration);
            var str20 = string.Format(txt, "isEatingStuff", playerEntity.abilityData.isEatingStuff);
            var str21 = string.Format(txt, "reUseWallTimer", playerEntity.abilityData.reUseWallTimer);
            var str65 = string.Format(txt, "bufferAbility", playerEntity.abilityData.bufferAbility);
            var str66 = string.Format(txt, "bufferedParry", playerEntity.abilityData.bufferedParry);
            var str67 = string.Format(txt, "bufferedSpecial", playerEntity.abilityData.bufferedSpecial);
            var str22 = string.Format(txt, "prePosition", playerEntity.entityData.prePosition);
            var str23 = string.Format(txt, "heading", playerEntity.entityData.heading);
            var str24 = string.Format(txt, "velocity", playerEntity.entityData.velocity);
            var str25 = string.Format(txt, "preOnGround", playerEntity.entityData.preOnGround);
            var str26 = string.Format(txt, "onGround", playerEntity.entityData.onGround);
            var str27 = string.Format(txt, "onRightWall", playerEntity.entityData.onRightWall);
            var str28 = string.Format(txt, "onRightWallApprox", playerEntity.entityData.onRightWallApprox);
            var str29 = string.Format(txt, "onLeftWall", playerEntity.entityData.onLeftWall);
            var str30 = string.Format(txt, "onLeftWallApprox", playerEntity.entityData.onLeftWallApprox);
            var str31 = string.Format(txt, "onCeilingApprox", playerEntity.entityData.onCeilingApprox);
            var str32 = string.Format(txt, "active", playerEntity.entityData.active);
            var str33 = string.Format(txt, "canBeHitByPlayer", playerEntity.hitableData.canBeHitByPlayer);
            var str34 = string.Format(txt, "canBeHitByBall", playerEntity.hitableData.canBeHitByBall);
            var str35 = string.Format(txt, "canBeHitByItem", playerEntity.hitableData.canBeHitByItem);
            var str36 = string.Format(txt, "canHitPlayer", playerEntity.hitableData.canHitPlayer);
            var str37 = string.Format(txt, "hasStageCollision", playerEntity.hitableData.hasStageCollision);
            var str38 = string.Format(txt, "hp", playerEntity.hitableData.hp);
            var str42 = string.Format(txt, "hitstunState", playerEntity.hitableData.hitstunState);
            var str39 = string.Format(txt, "hitstunDuration", playerEntity.hitableData.hitstunDuration);
            var str43 = string.Format(txt, "hitstunTimer", playerEntity.hitableData.hitstunTimer);
            var str40 = string.Format(txt, "lastHitterIndex", playerEntity.hitableData.lastHitterIndex);
            var str41 = string.Format(txt, "lastVictimIndex", playerEntity.hitableData.lastVictimIndex);
            var str44 = string.Format(txt, "preHitstunVelocity", playerEntity.hitableData.preHitstunVelocity);
            var str45 = string.Format(txt, "flyDirection", playerEntity.hitableData.flyDirection);
            var str46 = string.Format(txt, "flySpeed", playerEntity.hitableData.flySpeed);
            var str47 = string.Format(txt, "fromShortendHitstun", playerEntity.hitableData.fromShortendHitstun);
            var str48 = string.Format(txt, "isDirect", playerEntity.hitableData.isDirect);
            var str49 = string.Format(txt, "team", playerEntity.hitableData.team);
            var str50 = string.Format(txt, "stocks", playerEntity.playerData.stocks);
            var str51 = string.Format(txt, "kills", playerEntity.playerData.kills);
            var str52 = string.Format(txt, "deaths", playerEntity.playerData.deaths);
            var str53 = string.Format(txt, "points", playerEntity.playerData.points);
            var str54 = string.Format(txt, "teamPoints", playerEntity.playerData.teamPoints);
            var str55 = string.Format(txt, "playerState", playerEntity.moveableData.playerState);
            var str56 = string.Format(txt, "abilityState", playerEntity.moveableData.abilityState);
            var str57 = string.Format(txt, "abilityStateTimer", playerEntity.moveableData.abilityStateTimer);
            var str58 = string.Format(txt, "extraJumps", playerEntity.moveableData.extraJumps);
            var str59 = string.Format(txt, "gravityOff", playerEntity.moveableData.gravityOff);
            var str60 = string.Format(txt, "hasExtraAirMove", playerEntity.moveableData.hasExtraAirMove);
            var str61 = string.Format(txt, "slideIntoAction", playerEntity.moveableData.slideIntoAction);
            var str62 = string.Format(txt, "slowDustToggle", playerEntity.moveableData.slowDustToggle);
            var str63 = string.Format(txt, "airControlTimer", playerEntity.moveableData.airControlTimer);
            var str64 = string.Format(txt, "powerup", playerEntity.moveableData.powerup);
            var str68 = string.Format(txt, "canBufferSpecial", (aState != null) ? playerEntity.GetCurrentAbilityState().canBufferSpecial.ToString() : "");
            var str69 = string.Format(txt, "hitSomething", (ability != null) ? playerEntity.GetCurrentAbility().data.hitSomething.ToString() : "");
            var str70 = string.Format(txt, "hitstunState", BallHandler.instance.GetBall().hitableData.hitstunState.ToString());
            var str71 = string.Format(txt, "worldMin", ConvertTo.Vector2(World.instance.stageMin).ToString());
            var str72 = string.Format(txt, "worldMax", ConvertTo.Vector2(World.instance.stageMax).ToString());
            var str73 = string.Format(txt, "recoveryTimer", ConvertTo.Float(playerEntity.abilityData.recoveryTimer) * 0.6f);
            var str74 = string.Format(txt, "ParryActiveTimer", ConvertTo.Float(playerEntity.abilityData.parryActiveTimer) * 0.6f);
            var str75 = string.Format(txt, "inParryExtendedHitpause", playerEntity.abilityData.inParryExtendedHitpause);
            var str76 = string.Format(txt, "Ability State Duration", ConvertTo.Float(playerEntity.GetCurrentAbilityState().duration) * 60f);

            GUIStyle style = new GUIStyle()
            {
                fontSize = 12,
            };

            style.normal.textColor = Color.white;

            GUI.DragWindow(new Rect(0, 0, windowRect.width, windowRect.height));
            GUILayout.BeginArea(new Rect(0, 0, windowRect.width, windowRect.height), ATStyle.border);
            scrollView = GUILayout.BeginScrollView(scrollView, ATStyle.MainStyle);
            GUILayout.Label(str1, style);
            GUILayout.Label(str2, style);
            GUILayout.Label(str4, style);
            GUILayout.Label(str5, style);
            GUILayout.Label(str6, style);
            GUILayout.Label(str7, style);
            GUILayout.Label(str8, style);
            GUILayout.Label(str9, style);
            GUILayout.Label(str10, style);
            GUILayout.Label(str11, style);
            GUILayout.Label(str12, style);
            GUILayout.Label(str13, style);
            GUILayout.Label(str14, style);
            GUILayout.Label(str15, style);
            GUILayout.Label(str16, style);
            GUILayout.Label(str17, style);
            GUILayout.Label(str18, style);
            GUILayout.Label(str19, style);
            GUILayout.Label(str20, style);
            GUILayout.Label(str21, style);
            GUILayout.Label(str22, style);
            GUILayout.Label(str23, style);
            GUILayout.Label(str24, style);
            GUILayout.Label(str25, style);
            GUILayout.Label(str26, style);
            GUILayout.Label(str27, style);
            GUILayout.Label(str28, style);
            GUILayout.Label(str29, style);
            GUILayout.Label(str30, style);
            GUILayout.Label(str31, style);
            GUILayout.Label(str32, style);
            GUILayout.Label(str33, style);
            GUILayout.Label(str34, style);
            GUILayout.Label(str35, style);
            GUILayout.Label(str36, style);
            GUILayout.Label(str37, style);
            GUILayout.Label(str38, style);
            GUILayout.Label(str39, style);
            GUILayout.Label(str41, style);
            GUILayout.Label(str42, style);
            GUILayout.Label(str43, style);
            GUILayout.Label(str44, style);
            GUILayout.Label(str45, style);
            GUILayout.Label(str46, style);
            GUILayout.Label(str47, style);
            GUILayout.Label(str48, style);
            GUILayout.Label(str49, style);
            GUILayout.Label(str50, style);
            GUILayout.Label(str51, style);
            GUILayout.Label(str52, style);
            GUILayout.Label(str53, style);
            GUILayout.Label(str54, style);
            GUILayout.Label(str55, style);
            GUILayout.Label(str56, style);
            GUILayout.Label(str57, style);
            GUILayout.Label(str58, style);
            GUILayout.Label(str59, style);
            GUILayout.Label(str60, style);
            GUILayout.Label(str61, style);
            GUILayout.Label(str62, style);
            GUILayout.Label(str63, style);
            GUILayout.Label(str64, style);
            GUILayout.Label(str65, style);
            GUILayout.Label(str66, style);
            GUILayout.Label(str67, style);
            GUILayout.Label(str68, style);
            GUILayout.Label(str69, style);
            GUILayout.Label(str70, style);
            GUILayout.Label(str71, style);
            GUILayout.Label(str72, style);
            GUILayout.Label(str73, style);
            GUILayout.Label(str74, style);
            GUILayout.Label(str75, style);
            GUILayout.Label(str76, style);

            foreach (Rewired.ActionElementMap name in InputHandler.GetLastActiveController().GetMap().AllMaps)
            {
                GUILayout.Label($"{name}", ATStyle.LabStyleLeft);
            }


            GUILayout.FlexibleSpace();
            GUILayout.EndScrollView();
            GUILayout.EndArea();

        }
        private void BallInfoMenu(int wId)
        {
            ALDOKEMAOMB player = ALDOKEMAOMB.BJDPHEHJJJK(0);
            PlayerEntity playerEntity = player.JCCIAMJEODH;
            AbilityState aState = playerEntity.GetCurrentAbilityState();
            Ability ability = playerEntity.GetCurrentAbility();
            BallEntity ball = BallHandler.instance.GetBall();

            string txt = "[{0}]: {1}";
            var str22 = string.Format(txt, "prePosition", ball.entityData.prePosition.GCPKPHMKLBN);
            var str23 = string.Format(txt, "heading", ball.entityData.heading + " " + ball.entityData.prePosition.CGJJEHPPOAN);
            var str24 = string.Format(txt, "velocity", ball.entityData.velocity);
            var str25 = string.Format(txt, "preOnGround", ball.entityData.preOnGround);
            var str26 = string.Format(txt, "onGround", ball.entityData.onGround);
            var str27 = string.Format(txt, "onRightWall", ball.entityData.onRightWall);
            var str28 = string.Format(txt, "onRightWallApprox", ball.entityData.onRightWallApprox);
            var str29 = string.Format(txt, "onLeftWall", ball.entityData.onLeftWall);
            var str30 = string.Format(txt, "onLeftWallApprox", ball.entityData.onLeftWallApprox);
            var str31 = string.Format(txt, "onCeilingApprox", ball.entityData.onCeilingApprox);
            var str32 = string.Format(txt, "active", ball.entityData.active);
            var str33 = string.Format(txt, "canBeHitByPlayer", ball.hitableData.canBeHitByPlayer);
            var str34 = string.Format(txt, "canBeHitByBall", ball.hitableData.canBeHitByBall);
            var str35 = string.Format(txt, "canBeHitByItem", ball.hitableData.canBeHitByItem);
            var str36 = string.Format(txt, "canHitPlayer", ball.hitableData.canHitPlayer);
            var str37 = string.Format(txt, "hasStageCollision", ball.hitableData.hasStageCollision);
            var str38 = string.Format(txt, "hp", ball.hitableData.hp);
            var str42 = string.Format(txt, "hitstunState", ball.hitableData.hitstunState);
            var str39 = string.Format(txt, "hitstunDuration", ball.hitableData.hitstunDuration);
            var str43 = string.Format(txt, "hitstunTimer", ball.hitableData.hitstunTimer);
            var str40 = string.Format(txt, "lastHitterIndex", ball.hitableData.lastHitterIndex);
            var str41 = string.Format(txt, "lastVictimIndex", ball.hitableData.lastVictimIndex);
            var str44 = string.Format(txt, "preHitstunVelocity", ball.hitableData.preHitstunVelocity);
            var str45 = string.Format(txt, "flyDirection", ConvertTo.Vector2(ball.hitableData.flyDirection));
            var str46 = string.Format(txt, "flySpeed", ball.hitableData.flySpeed);
            var str47 = string.Format(txt, "fromShortendHitstun", ball.hitableData.fromShortendHitstun);
            var str48 = string.Format(txt, "isDirect", ball.hitableData.isDirect);
            var str49 = string.Format(txt, "team", ball.hitableData.team);
            var str50 = string.Format(txt, "combo", ball.ballData.combo);
            var str51 = string.Format(txt, "ballAngle", ball.ballData.ballAngle);
            var str52 = string.Format(txt, "canHitPlayer", ball.bouncingData.canHitPlayer);
            var str53 = string.Format(txt, "specialHitHoriWall", ball.bouncingData.specialHitHoriWall);
            var str54 = string.Format(txt, "specialHitVertWall", ball.bouncingData.specialHitVertWall);
            var str55 = string.Format(txt, "ballState", ball.bouncingData.ballState);
            var str56 = string.Format(txt, "fromSpecial", ball.bouncingData.fromSpecial);
            var str57 = string.Format(txt, "fromGrabDeny", ball.bouncingData.fromGrabDeny);
            var str58 = string.Format(txt, "fromCounter", ball.bouncingData.fromCounter);
            var str59 = string.Format(txt, "fromDownBunt", ball.bouncingData.fromDownBunt);
            var str60 = string.Format(txt, "wallGrindSide", ball.bouncingData.wallGrindSide);
            var str61 = string.Format(txt, "lastWall", ball.bouncingData.lastWall);
            var str62 = string.Format(txt, "specialTimeSinceHit", ball.bouncingData.specialTimeSinceHit);
            var str63 = string.Format(txt, "timeSinceHit", ball.bouncingData.timeSinceHit);
            var str64 = string.Format(txt, "hitHorizontalWall", ball.bouncingData.hitHorizontalWall);
            var str65 = string.Format(txt, "showDirection", ball.bouncingData.showDirection);
            var str66 = string.Format(txt, "ballScale", ball.bouncingData.ballScale);
            var str67 = string.Format(txt, "pongTurnSide", ball.bouncingData.pongTurnSide);
            var str68 = string.Format(txt, "pongAim", ball.bouncingData.pongAim);
            var str69 = string.Format(txt, "turnSpeed", ball.bouncingData.turnSpeed);
            var str70 = string.Format(txt, "FDirToAngle", Math.FlyDirectionToAngle(ball.hitableData.flyDirection));
            var str71 = string.Format(txt, "FDirToAngle", Math.FlyDirectionToAngle(new IBGCBLLKIHA(HHBCPNCDNDH.GANELPBAOPN(ball.hitableData.flyDirection.CGJJEHPPOAN), ball.hitableData.flyDirection.GCPKPHMKLBN)));
            var str72 = string.Format(txt, "A2D X", Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.JCCIAMJEODH.hitAngleUp)).GCPKPHMKLBN);
            var str73 = string.Format(txt, "A2D Y", Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.JCCIAMJEODH.hitAngleUp)).CGJJEHPPOAN);
            var str74 = string.Format(txt, "D2A 1", Math.DirectionToAngle(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.JCCIAMJEODH.hitAngleUp)), HHBCPNCDNDH.NKKIFJJEPOL(1m)));
            var str75 = string.Format(txt, "D2A 0.5", Math.DirectionToAngle(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.JCCIAMJEODH.hitAngleUp)), HHBCPNCDNDH.NKKIFJJEPOL(0.5m)));
            var str76 = string.Format(txt, "D2A Pi/0.9", Math.DirectionToAngle(Math.AngleToDirection(HHBCPNCDNDH.NKKIFJJEPOL(player.JCCIAMJEODH.hitAngleUp)), HHBCPNCDNDH.NKKIFJJEPOL((decimal)Mathf.PI / 0.9m)));
            var str77 = string.Format(txt, "flyDirectionX", ConvertTo.Float(ball.hitableData.flyDirection.GCPKPHMKLBN));
            var str78 = string.Format(txt, "flyDirectionY", ConvertTo.Float(ball.hitableData.flyDirection.CGJJEHPPOAN));

            GUIStyle style = new GUIStyle()
            {
                fontSize = 12,
            };

            style.normal.textColor = Color.white;

            GUI.DragWindow(new Rect(0, 0, windowRect.width, windowRect.height));
            GUILayout.BeginArea(new Rect(0, 0, windowRect.width, windowRect.height), ATStyle.border);
            GUILayout.BeginVertical(ATStyle.MainStyle);
            GUILayout.Label(str22, style);
            GUILayout.Label(str23, style);
            GUILayout.Label(str24, style);
            GUILayout.Label(str25, style);
            GUILayout.Label(str26, style);
            GUILayout.Label(str27, style);
            GUILayout.Label(str28, style);
            GUILayout.Label(str29, style);
            GUILayout.Label(str30, style);
            GUILayout.Label(str31, style);
            GUILayout.Label(str32, style);
            GUILayout.Label(str33, style);
            GUILayout.Label(str34, style);
            GUILayout.Label(str35, style);
            GUILayout.Label(str36, style);
            GUILayout.Label(str37, style);
            GUILayout.Label(str38, style);
            GUILayout.Label(str39, style);
            GUILayout.Label(str40, style);
            GUILayout.Label(str41, style);
            GUILayout.Label(str42, style);
            GUILayout.Label(str43, style);
            GUILayout.Label(str44, style);
            GUILayout.Label(str45, style);
            GUILayout.Label(str46, style);
            GUILayout.Label(str47, style);
            GUILayout.Label(str48, style);
            GUILayout.Label(str49, style);
            GUILayout.Label(str50, style);
            GUILayout.Label(str51, style);
            GUILayout.Label(str52, style);
            GUILayout.Label(str53, style);
            GUILayout.Label(str54, style);
            GUILayout.Label(str55, style);
            GUILayout.Label(str56, style);
            GUILayout.Label(str57, style);
            GUILayout.Label(str58, style);
            GUILayout.Label(str59, style);
            GUILayout.Label(str60, style);
            GUILayout.Label(str61, style);
            GUILayout.Label(str62, style);
            GUILayout.Label(str63, style);
            GUILayout.Label(str64, style);
            GUILayout.Label(str65, style);
            GUILayout.Label(str66, style);
            GUILayout.Label(str67, style);
            GUILayout.Label(str68, style);
            GUILayout.Label(str69, style);
            GUILayout.Label(str70, style);
            GUILayout.Label(str71, style);
            GUILayout.Label(str72, style);
            GUILayout.Label(str73, style);
            GUILayout.Label(str74, style);
            GUILayout.Label(str75, style);
            GUILayout.Label(str76, style);
            GUILayout.Label(str77, style);
            GUILayout.Label(str78, style);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.EndArea();

        }

        private void DebugMenu(int wId)
        {
            GUIStyle style = new GUIStyle()
            {
                fontSize = 24,
                fixedHeight = 36,
                margin = new RectOffset(5, 0, 4, 4),
                alignment = TextAnchor.MiddleLeft
            };
            style.normal.textColor = Color.white;
            style.normal.background = colours[4];

            GUIStyle winStyle = new GUIStyle();
            winStyle.normal.background = colours[4];

            GUIStyle btnStyle = new GUIStyle(GUI.skin.button)
            {
                fixedHeight = 36,
                fixedWidth = 64

            };

            GUILayout.BeginArea(new Rect(0, 20, windowRect.width, windowRect.height), winStyle);
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Label("Don't Lose HP", style);
            GUILayout.Label("Don't Get Hit", style);
            GUILayout.Label("\"Infinite\" Energy", style);
            GUILayout.EndVertical();
            GUILayout.BeginVertical();
            DebugSettings.instance.dontLoseHP = GUILayout.Toggle(DebugSettings.instance.dontLoseHP, DebugSettings.instance.dontLoseHP.ToString(), btnStyle);
            DebugSettings.instance.dontGetHit = GUILayout.Toggle(DebugSettings.instance.dontGetHit, DebugSettings.instance.dontGetHit.ToString(), btnStyle);
            DebugSettings.instance.noEnergyRequiredForSpecial = GUILayout.Toggle(DebugSettings.instance.noEnergyRequiredForSpecial, DebugSettings.instance.noEnergyRequiredForSpecial.ToString(), btnStyle);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            GUI.DragWindow(new Rect(0, 0, windowRect.width, windowRect.height));
        }

        void DisableControllerExpress(int mapID)
        {
            string txt = "";
            foreach (var name in Rewired.ReInput.mapping.GetControllerMap(mapID).AllMaps)
            {
                switch (name.actionDescriptiveName)
                {
                    case "Up":
                        name.keyboardKeyCode = Rewired.KeyboardKeyCode.W;
                        continue;
                    case "Down":
                        name.keyboardKeyCode = Rewired.KeyboardKeyCode.F;
                        continue;
                    case "Left":
                        name.keyboardKeyCode = Rewired.KeyboardKeyCode.Q;
                        continue;
                    case "Right":
                        name.keyboardKeyCode = Rewired.KeyboardKeyCode.E;
                        continue;
                    default:
                        break;
                }
                txt += $"{name}\n";
            }

            AdvancedTraining.Log.LogDebug(txt);
        }

        bool DoOnce = false;
        private void InputWindow(int wId)
        {
            BallEntity ball = BallHandler.instance.GetBall();
            ALDOKEMAOMB player = ALDOKEMAOMB.BJDPHEHJJJK(wId - 6);
            string txt = "{0} : {1}";

            GUIStyle style = new GUIStyle()
            {
                fontSize = 12,
                padding = new RectOffset(4, 0, 0, 0)
            };

            GUIStyle headerStyle = new GUIStyle(GUI.skin.label)
            {
                fontSize = 15,
            };

            GUIStyle areaStyle = new GUIStyle()
            {
                padding = new RectOffset(5, 5, 24, 5),
            };

            GUIStyle winStyle = new GUIStyle()
            {
                padding = new RectOffset(5, 5, 5, 5),
            };

            style.normal.textColor = Color.white;
            style.normal.background = colours[4];
            winStyle.normal.background = colours[4];

            GUI.DragWindow(new Rect(0, 0, inputRect.width, inputRect.height));
            GUILayout.BeginArea(new Rect(0, 0, inputRect.width, inputRect.height), ATStyle.border);
            GUILayout.BeginHorizontal(ATStyle.MainStyle);
            GUILayout.BeginVertical();
            GUILayout.Label("Movement/Aim Keys", headerStyle);
            GUILayout.Label(string.Format(txt, "LEFT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.LEFT)]), style);
            GUILayout.Label(string.Format(txt, "RIGHT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.RIGHT)]), style);
            GUILayout.Label(string.Format(txt, "UP", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.UP)]), style);
            GUILayout.Label(string.Format(txt, "DOWN", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.DOWN)]), style);
            GUILayout.Label(string.Format(txt, "JUMP", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.JUMP)]), style);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.Label("Combat Keys", headerStyle);
            GUILayout.Label(string.Format(txt, "SWING", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.SWING)]), style);
            GUILayout.Label(string.Format(txt, "GRAB", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.GRAB)]), style);
            GUILayout.Label(string.Format(txt, "BUNT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.BUNT)]), style);
            GUILayout.Label(string.Format(txt, "TAUNT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.TAUNT)]), style);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.Label("Express Keys", headerStyle);
            GUILayout.Label(string.Format(txt, "EXPRESS_LEFT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.EXPRESS_LEFT)]), style);
            GUILayout.Label(string.Format(txt, "EXPRESS_RIGHT", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.EXPRESS_RIGHT)]), style);
            GUILayout.Label(string.Format(txt, "EXPRESS_UP", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.EXPRESS_UP)]), style);
            GUILayout.Label(string.Format(txt, "EXPRESS_DOWN", InputHandler.currentInput[player.CJFLMDNNMIE, InputAction.ActionToIndex(InputAction.EXPRESS_DOWN)]), style);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

        }

        public Texture2D ColorToTexture2D(Color color)
        {
            Texture2D texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture2D.SetPixel(0, 0, color);
            texture2D.Apply();
            return texture2D;
        }

        WorldData saveState1;
        int saveFrame1;
        WorldData saveState2;
        int saveFrame2;
        WorldData saveState3;
        int saveFrame3;

        public int atSaveState(ref WorldData savestate)
        {
            if (savestate == null)
            {
                savestate = new WorldData();
            }
            savestate.NDEEDGCMMOC(World.instance.worldData, true);
            //GameStatesGame.curFrame
            return OGONAGCFDPK.DDBJKEIHELD;
        }

        public void atLoadState(WorldData worldData, int saveStateFrame)
        {
            if (worldData != null)
            {
                World.instance.worldData.NDEEDGCMMOC(worldData, false);
            }
            //GameStatesGame.SetCurFrame
            ScreenGameHud.UpdateSpeed(BallHandler.instance.GetBall(0).ballData.flySpeed);
            OGONAGCFDPK.HIMHHFDMOEJ(saveStateFrame);
            World.instance.LoadedState();
            World.instance.FrameUpdate();
        }

        Rect charaRect = new Rect(Screen.width - 350, Screen.height - 1000, 350, 1000);
        Vector2 mainScrollView;
        List<string> currentAbilityList = new List<string>();

        bool[] showMovement = new bool[5];
        bool showAbilityState;
        bool showCurrentAbility;
        bool showStats;
        bool showAngles;
        bool hideCharacterInfo;
        bool showHurtbox;
        bool showHitbox;
        bool[] showBall = new bool[5];
        bool showPlayer;
        bool showSaveStates;
        private bool showCharacterInfo;
        Vector2[] debugScroll;

        void charaInfo(int wId)
        {
            ALDOKEMAOMB player = ALDOKEMAOMB.BJDPHEHJJJK(0);
            PlayerEntity playerEntity = player.JCCIAMJEODH;
            BallEntity ball = BallHandler.instance.GetBall(0);
            SkatePlayer skate = FindObjectOfType<SkatePlayer>();
            BossPlayer boss = FindObjectOfType<BossPlayer>();
            BagPlayer bag = FindObjectOfType<BagPlayer>();
            CrocPlayer croc = FindObjectOfType<CrocPlayer>();
            CopPlayer cop = FindObjectOfType<CopPlayer>();
            List<string> tempCurAbilityList = new List<string>();
            List<string> abilityList = new List<string>();
            List<string> abilityStateList = new List<string>();
            List<string> hurtBoxList = new List<string>();
            List<string> hitBoxList = new List<string>();

            if (playerEntity.GetCurrentAbility() != null && showCurrentAbility)
            {
                foreach (string name in playerEntity.GetCurrentAbility().abilityStatesSequence)
                {
                    tempCurAbilityList.Add(string.Format($"{playerEntity.GetAbilityState(name).name} : <b>{Mathf.Ceil(ConvertTo.Time60Frames(playerEntity.GetAbilityState(name).duration))}</b>"));
                }
                if (currentAbilityList != tempCurAbilityList)
                {
                    currentAbilityList = tempCurAbilityList;
                }
            }

            if (showAbilityState)
            {
                foreach (KeyValuePair<string, Ability> keyValuePair in playerEntity.abilities)
                {
                    abilityList.Add(string.Format($"\n<b>―――― {keyValuePair.Key} ――――</b>"));
                    foreach (string name in playerEntity.GetAbility(keyValuePair.Key).abilityStatesSequence)
                    {
                        abilityList.Add(string.Format($"{playerEntity.GetAbilityState(name).name} : <b>{ConvertTo.Time60Frames(playerEntity.GetAbilityState(name).duration)}</b>"));
                    }
                }
            }

            if (showAbilityState)
            {
                foreach (KeyValuePair<string, AbilityState> keyValuePair2 in playerEntity.abilityStates)
                {
                    if (ConvertTo.Float(playerEntity.GetAbilityState(keyValuePair2.Key).duration) != 0f)
                    {
                        abilityStateList.Add(string.Concat(new string[]
                        {
                            string.Format($"{playerEntity.GetAbilityState(keyValuePair2.Key).name} : <b>{ConvertTo.Time60Frames(playerEntity.GetAbilityState(keyValuePair2.Key).duration)}</b>"),
                        }));
                        foreach (string item in abilityList)
                        {
                            if (abilityStateList.Contains(item))
                            {
                                abilityStateList.Remove(item);
                            }
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, PlayerBox> playerBox in playerEntity.hurtboxes)
            {
                IBGCBLLKIHA hubSize = playerBox.Value.bounds.IACOKHPMNGN;
                hurtBoxList.Add(string.Concat(new string[]
                {
                    string.Format($"{playerBox.Key} : <b>{ConvertTo.Float(ConvertTo.Multiply(hubSize.GCPKPHMKLBN,60f))}, {ConvertTo.Float(ConvertTo.Multiply(hubSize.CGJJEHPPOAN,60f))}</b>"),
                }));
            }

            foreach (KeyValuePair<string, PlayerHitbox> playerHitBox in playerEntity.hitboxes)
            {
                IBGCBLLKIHA hubSize = playerHitBox.Value.bounds.IACOKHPMNGN;
                hitBoxList.Add(string.Concat(new string[]
                {
                    string.Format($"{playerHitBox.Key} : <b>{ConvertTo.Float(ConvertTo.Multiply(hubSize.GCPKPHMKLBN,60f))}, {ConvertTo.Float(ConvertTo.Multiply(hubSize.CGJJEHPPOAN,60f))}</b>"),
                }));
            }

            GUIStyle abilityStyle = new GUIStyle()
            {
                fixedHeight = 100,
            };

            GUIStyle border = new GUIStyle()
            {
                padding = new RectOffset(10, 0, 24, 10),
            };
            GUILayout.BeginArea(new Rect(0, 0, charaRect.width, charaRect.height), border);
            mainScrollView = GUILayout.BeginScrollView(mainScrollView, false, true, new GUIStyle(GUI.skin.horizontalScrollbar), new GUIStyle(GUI.skin.verticalScrollbar));
            GUILayout.BeginVertical(ATStyle.BgStyle);
            if (showBall[0] = GUILayout.Toggle(showBall[0], "Ball Info", ATStyle.LabdivStyle))
            {
                if (showBall[1] = GUILayout.Toggle(showBall[1], "Position Data", ATStyle.SplitterStyle))
                {
                    GUILayout.Label($"Pos-X : <b>{ConvertTo.Float(ConvertTo.Multiply(ball.ballData.prePosition.GCPKPHMKLBN, 60))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Pos-Y : <b>{ConvertTo.Float(ConvertTo.Multiply(ball.ballData.prePosition.CGJJEHPPOAN, 60))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Pixel Pos-X : <b>{ConvertTo.Float(ball.ballData.prePosition.GCPKPHMKLBN)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Pixel Pos-Y : <b>{ConvertTo.Float(ball.ballData.prePosition.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);
                }
                if (showBall[2] = GUILayout.Toggle(showBall[2], "Hitting Data", ATStyle.SplitterStyle))
                {
                    GUILayout.Label($"Ball State : <b>{ball.ballData.ballState}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Hitstun State : <b>{ball.hitableData.hitstunState}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Shortened Hitstun? : <b>{ball.hitableData.fromShortendHitstun}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Hitstun Duration : <b>{ConvertTo.Time60Frames(ball.hitableData.hitstunDuration)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Hitstun Timer : <b>{ConvertTo.Time60Frames(ball.hitableData.hitstunTimer)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Damage : <b>{ConvertTo.Float(ball.GetDamage(BGHNEHPFHGC.NMJDMHNMDNJ))}</b>", ATStyle.LabStyleLeft);
                }
                if (showBall[3] = GUILayout.Toggle(showBall[3], "Moving Data", ATStyle.SplitterStyle))
                {
                    GUILayout.Label($"Velocity-X : <b>{ConvertTo.Float(ball.entityData.velocity.GCPKPHMKLBN)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Velocity-Y : <b>{ConvertTo.Float(ball.entityData.velocity.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Fly Speed : <b>{ConvertTo.Float(ball.hitableData.flySpeed)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Pixel Fly Speed : <b>{ConvertTo.Float(ConvertTo.Divide(ball.hitableData.flySpeed, 0.6f))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Time Since Hit : <b>{ConvertTo.Time60Frames(ball.bouncingData.timeSinceHit)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Combo : <b>{ball.ballData.combo}</b>", ATStyle.LabStyleLeft);
                }
            }

            showPlayer = GUILayout.Toggle(showPlayer, "Player Info", ATStyle.LabdivStyle);
            if (showPlayer)
            {
                PlayerEntity entity = playerEntity;
                GUILayout.Label($"Heading : <b>{entity.entityData.heading}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Player State : <b>{entity.moveableData.playerState}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Ability State : <b>{entity.moveableData.abilityState}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Ability State Timer : <b>{ConvertTo.Time60Frames(entity.moveableData.abilityStateTimer)}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Hitpause State : <b>{entity.attackingData.hitPauseState}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Hitstun Timer : <b>{ConvertTo.Time60Frames(entity.hitableData.hitstunTimer)}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Hitstun Duration : <b>{ConvertTo.Time60Frames(entity.hitableData.hitstunDuration)}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Special Amount : <b>{entity.playerData.specialAmount}</b>", ATStyle.LabStyleLeft);
            }

            showStats = GUILayout.Toggle(showStats, "Stats", ATStyle.LabdivStyle);
            if (showStats)
            {
                GUILayout.Label(string.Format($"Pos-X : <b>{ConvertTo.Float(playerEntity.moveableData.prePosition.GCPKPHMKLBN) * 60}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Pos-Y : <b>{ConvertTo.Float(playerEntity.moveableData.prePosition.CGJJEHPPOAN) * 60}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Big Hits : <b>{playerEntity.playerData.bigHitsCount}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Big Hit Multiplier : <b>x{Mathf.Max(ConvertTo.Float(playerEntity.playerData.bigHitsCountAdditional) + 1, 1.1f)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Hitstun Duration : <b>{ConvertTo.Float(playerEntity.playerData.hitstunDuration) * 60}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Hitstun Timer : <b>{ConvertTo.Float(playerEntity.playerData.hitstunTimer) * 60}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Parry Buffered : <b>{playerEntity.playerData.bufferedParry}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Special Buffered : <b>{playerEntity.playerData.bufferedSpecial}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Charge Max Duration : <b>{ConvertTo.Time60Frames(playerEntity.chargeMaxDuration)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Full Charge Margin : <b>{ConvertTo.Time60Frames(playerEntity.fullChargeMargin)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Charge Power : <b>{ConvertTo.Time60Frames(playerEntity.playerData.chargePower)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Charge Speed Additional: <b>{ConvertTo.Int(ConvertTo.Multiply(ConvertTo.Divide(playerEntity.attackingData.chargePower, playerEntity.chargeMaxDuration), HHBCPNCDNDH.NKKIFJJEPOL(10.0m)))}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Throw Height : <b>{playerEntity.pxHeight / 2 + ConvertTo.Float(playerEntity.throwOffset.CGJJEHPPOAN) * 100}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Hit Something : <b>{playerEntity.abilityData.hitSomething}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Height : <b>{playerEntity.pxHeight}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"World State : <b>{World.state}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Stage Size : <b>{ConvertTo.Float(ConvertTo.Multiply(World.instance.stageSize.GCPKPHMKLBN, 60))}, {ConvertTo.Float(ConvertTo.Multiply(World.instance.stageSize.CGJJEHPPOAN, 60))}</b>"), ATStyle.LabStyleLeft);
            }

            showAngles = GUILayout.Toggle(showAngles, "Angles", ATStyle.LabdivStyle);
            if (showAngles)
            {
                GUILayout.Label(string.Format($"Up : <b>{ConvertTo.EzAngle(playerEntity.hitAngleUp)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Down : <b>{ConvertTo.EzAngle(playerEntity.hitAngleDown)}</b>"), ATStyle.LabStyleLeft);
                if (playerEntity.hitAngleDown != playerEntity.hitAngleNeutralDownAir) GUILayout.Label(string.Format($"AIR-DOWN : <b>{ConvertTo.EzAngle(playerEntity.hitAngleNeutralDownAir)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Smash : <b>{ConvertTo.EzAngle(playerEntity.hitAngleSmash)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Spike-F : <b>{ConvertTo.EzAngle(playerEntity.hitAngleDownAirForward)}</b>"), ATStyle.LabStyleLeft);
                GUILayout.Label(string.Format($"Spike-B : <b>{ConvertTo.EzAngle(playerEntity.hitAngleDownAirBackward)}</b>"), ATStyle.LabStyleLeft);
                if (croc != null) GUILayout.Label(string.Format($"Wall Swing Down : <b>{ConvertTo.EzAngle(ConvertTo.Int(croc.wallClimbDownAim))}</b>"), ATStyle.LabStyleLeft);
                if (croc != null) GUILayout.Label(string.Format($"Spit Down-Forward : <b>{ConvertTo.EzAngle(18)}</b>"), ATStyle.LabStyleLeft);
                if (cop != null) GUILayout.Label(string.Format($"Cuff Down : <b>{ConvertTo.EzAngle(ConvertTo.Int(cop.cuffAngleDown))}</b>"), ATStyle.LabStyleLeft);
                if (playerEntity.character == Character.BOOM) GUILayout.Label(string.Format($"The Beat : <b>{ConvertTo.EzAngle(315)}</b>"), ATStyle.LabStyleLeft);
                if (playerEntity.character == Character.PONG) GUILayout.Label(string.Format($"Ground Spin : <b>{ConvertTo.EzAngle(352)}</b>"), ATStyle.LabStyleLeft);
                if (playerEntity.character == Character.PONG) GUILayout.Label(string.Format($"Ceiling Spin : <b>{ConvertTo.EzAngle(45)}</b>"), ATStyle.LabStyleLeft);
            }

            if (showMovement[0] = GUILayout.Toggle(showMovement[0], "Movement", ATStyle.LabdivStyle))
            {
                GUILayout.Label($"Velocity-X : <b>{ConvertTo.Float(playerEntity.moveableData.velocity.GCPKPHMKLBN)}</b>", ATStyle.LabStyleLeft);
                GUILayout.Label($"Velocity-Y : <b>{ConvertTo.Float(playerEntity.moveableData.velocity.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);

                if (showMovement[1] = GUILayout.Toggle(showMovement[1], "Ground Data", ATStyle.SplitterStyle))
                {
                    GUILayout.Label($"Ground Initial Speed: <b>{ConvertTo.Float(ConvertTo.Multiply(playerEntity.maxMove, HHBCPNCDNDH.NKKIFJJEPOL(0.5m)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Ground Accelaration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.groundAcc, ConvertTo.Floatf(60)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Ground Decelaration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.groundDeacc, ConvertTo.Floatf(60)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Ground Top Speed : <b>{ConvertTo.Float(ConvertTo.Add(playerEntity.maxMove, HHBCPNCDNDH.NKKIFJJEPOL(0.3m)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Slide Decelaration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.slideDeacc, ConvertTo.Floatf(60)))}</b>", ATStyle.LabStyleLeft);
                }
                if (showMovement[2] = GUILayout.Toggle(showMovement[2], "Air Data", ATStyle.SplitterStyle))
                {
                    GUILayout.Label($"Air Accelaration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.airAcc, ConvertTo.Floatf(60)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Air Decelaration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.airDeacc, ConvertTo.Floatf(60)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Air Top Speed : <b>{ConvertTo.Float(ConvertTo.Add(playerEntity.maxAirMove, HHBCPNCDNDH.NKKIFJJEPOL(0.3m)))}</b>", ATStyle.LabStyleLeft);
                }
                if (showMovement[3] = GUILayout.Toggle(showMovement[3], "Fall Data", ATStyle.SplitterStyle))
                {
                    GUILayout.Label($"Fall Accelaration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForce, ConvertTo.Floatf(60)))}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Fall Top Speed : <b>{ConvertTo.Float(playerEntity.maxGravity)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Fast-Fall Speed : <b>{ConvertTo.Float(playerEntity.gravityForceFastFall)}</b>", ATStyle.LabStyleLeft);
                    if (bag != null) GUILayout.Label(string.Format("{0}", string.Format($"Kite Duration : <b>{ConvertTo.Float(bag.maxKiteFuel)} Sec</b>")), ATStyle.LabStyleLeft);
                }
                if (showMovement[4] = GUILayout.Toggle(showMovement[4], "Jump Data", ATStyle.SplitterStyle))
                {
                    if (skate != null) GUILayout.Label(string.Format("{0}", string.Format($"Hover Duration : <b>{ConvertTo.Float(skate.maxFuel)} Sec</b>")), ATStyle.LabStyleLeft);
                    if (boss != null) GUILayout.Label(string.Format("{0}", string.Format($"Fly Duration : <b>{ConvertTo.Float(boss.maxFuel)} Sec</b>")), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Jump Deceleration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceUp, ConvertTo.Floatf(60)))}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Jump Power : <b>{ConvertTo.Float(playerEntity.jumpPower)}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Extra Jump Power : <b>{ConvertTo.Float(playerEntity.extraJumpPower)}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Extra Jump Amount : <b>{playerEntity.extraJumpAmount}</b>"), ATStyle.LabStyleLeft);
                    if (!ConvertTo.Equal(playerEntity.highJumpPower, HHBCPNCDNDH.DBOMOJGKIFI)) GUILayout.Label(string.Format($"Super Jump Power : <b>{ConvertTo.Float(playerEntity.highJumpPower)}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Apex Deceleration : <b>{ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceApex, ConvertTo.Floatf(60)))}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Apex In : <b>{ConvertTo.Float(playerEntity.apexIn)}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Apex Out : <b>{ConvertTo.Float(playerEntity.apexOut)}</b>"), ATStyle.LabStyleLeft);
                    float jumpHeight = 0;
                    float extrajumpHeight = 0;
                    for (float i = ConvertTo.Float(playerEntity.jumpPower); i > 0; i = (i < ConvertTo.Float(playerEntity.apexIn)) ? i - ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceApex, ConvertTo.Floatf(60))) : i - ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceUp, ConvertTo.Floatf(60))))
                    {
                        jumpHeight += i;
                    }
                    for (float i = ConvertTo.Float(playerEntity.extraJumpPower); i > 0; i = (i < ConvertTo.Float(playerEntity.apexIn)) ? i - ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceApex, ConvertTo.Floatf(60))) : i - ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceUp, ConvertTo.Floatf(60))))
                    {
                        extrajumpHeight += i;
                    }
                    GUILayout.Label(string.Format($"Jump Height : <b>{jumpHeight}</b>"), ATStyle.LabStyleLeft);
                    GUILayout.Label(string.Format($"Extra Jump Height : <b>{extrajumpHeight}</b>"), ATStyle.LabStyleLeft);
                    if (!ConvertTo.Equal(playerEntity.highJumpPower, HHBCPNCDNDH.DBOMOJGKIFI))
                    {
                        float highJumpHeight = 0;
                        for (float i = ConvertTo.Float(playerEntity.highJumpPower); i > 0; i = (i < ConvertTo.Float(playerEntity.apexIn)) ? i - ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceApex, ConvertTo.Floatf(60))) : i - ConvertTo.Float(ConvertTo.Divide(playerEntity.gravityForceUp, ConvertTo.Floatf(60))))
                        {
                            highJumpHeight += i;
                        }
                        GUILayout.Label(string.Format($"Super Jump Height : <b>{highJumpHeight}</b>"), ATStyle.LabStyleLeft);
                    }
                }
            }

            showCurrentAbility = GUILayout.Toggle(showCurrentAbility, "Current Ability", ATStyle.LabdivStyle);
            if (showCurrentAbility)
            {
                GUILayout.BeginVertical(abilityStyle);
                foreach (string name in currentAbilityList)
                {
                    GUILayout.Label(name, ATStyle.LabStyleLeft);
                }
                GUILayout.EndVertical();
            }

            showAbilityState = GUILayout.Toggle(showAbilityState, "Ability States", ATStyle.LabdivStyle);
            if (showAbilityState)
            {
                foreach (string name in abilityStateList)
                {
                    GUILayout.Label(name, ATStyle.LabStyleLeft);
                }
                foreach (string name in abilityList)
                {
                    GUILayout.Label(name, ATStyle.LabStyleLeft);
                }
            }

            showHurtbox = GUILayout.Toggle(showHurtbox, "Hurt Boxes", ATStyle.LabdivStyle);
            if (showHurtbox)
            {
                foreach (string name in hurtBoxList)
                {
                    GUILayout.Label(name, ATStyle.LabStyleLeft);
                }
            }

            showHitbox = GUILayout.Toggle(showHitbox, "Hit Boxes", ATStyle.LabdivStyle);
            if (showHitbox)
            {
                foreach (string name in hitBoxList)
                {
                    GUILayout.Label(name, ATStyle.LabStyleLeft);
                }


                foreach (string name in playerEntity.GetCurrentAbility().abilityStatesSequence)
                {
                    foreach (KeyValuePair<string, PlayerHitbox> keyValuePair2 in playerEntity.hitboxes)
                    {
                        if (playerEntity.GetCurrentAbilityState().hitboxes.Contains(keyValuePair2.Key) && playerEntity.GetCurrentAbilityState() != null)
                        {
                            AbilityState abilityState = playerEntity.GetAbilityState(name);
                            GUILayout.Label($"{name} : <b>{ConvertTo.Time60Frames(abilityState.duration)}</b> : <b>{abilityState.hitboxes.Contains(keyValuePair2.Key)}</b> : <b>{abilityState.hitboxes.Contains(keyValuePair2.Key) && !abilityState.hitboxesOffAfterHitSomething}</b>", ATStyle.LabStyleLeft);
                        }
                    }
                }

            }

            showSaveStates = GUILayout.Toggle(showSaveStates, "Save States", ATStyle.LabdivStyle);
            if (showSaveStates)
            {
                GUILayout.Label($"Current Frame : <b>{OGONAGCFDPK.DDBJKEIHELD}</b>", ATStyle.LabStyleLeft);
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Save 1", ATStyle.ActButton)) saveFrame1 = atSaveState(ref saveState1);
                if (GUILayout.Button("Load 1", ATStyle.DeactButton)) atLoadState(saveState1, saveFrame1);
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Save 2", ATStyle.ActButton)) saveFrame2 = atSaveState(ref saveState2);
                if (GUILayout.Button("Load 2", ATStyle.DeactButton)) atLoadState(saveState2, saveFrame2);
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Save 3", ATStyle.ActButton)) saveFrame3 = atSaveState(ref saveState3);
                if (GUILayout.Button("Load 3", ATStyle.DeactButton)) atLoadState(saveState3, saveFrame3);
                GUILayout.EndHorizontal();

                InputHandler.GetLastActiveController().GetMap();
            }

            hideCharacterInfo = GUILayout.Toggle(hideCharacterInfo, hideCharacterInfo ? "Show Character Info" : "Hide Character Info", ATStyle.LabdivStyle);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            GUI.DragWindow(new Rect(0, 0, charaRect.width, charaRect.height));

        }
    }
#endif
}
