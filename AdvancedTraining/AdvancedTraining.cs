﻿using BepInEx;
using BepInEx.Configuration;
using GameplayEntities;
using LLGUI;
using LLHandlers;
using System.Collections.Generic;
using UnityEngine;
using LLBML.Utils;
using BepInEx.Logging;

namespace AdvancedTraining
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfos.PLUGIN_GUID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION), BepInRef]
    public class AdvancedTraining : BaseUnityPlugin
    {
#pragma warning disable IDE0051 // Remove unused private members
        private const string modVersion = "2.0.2";
        private const string repositoryOwner = "Daioutzu";
        private const string repositoryName = "LLBMM-AdvancedTraining";
#pragma warning restore IDE0051

        public ALDOKEMAOMB playerMain { get; private set; }
        public PlayerEntity playerEntityMain { get; private set; }
        public BallEntity ball { get; private set; }

        public bool showInfo { get; private set; }
        public PlayerEntity Dummy { get; private set; }
        static public JOFJHDJHJGI CurrentGameState => DNPFJHMAIBP.HHMOGKIMBNM();
        static public GameMode CurrentGameMode => JOMBNFKIHIC.GIGAKBJGFDI.PNJOKAICMNN;

        internal delegate void InGameButtonPress(bool InGame);
        internal delegate void StandardEvent();
        internal event InGameButtonPress OnHitboxKeyDown, OnAngleDrawerKeyDown, OnFastFallIndicatorKeyDown, OnPartnerSelectKeyDown;
        internal event StandardEvent OnTrainingModeStart, OnTrainingModeEnd;
        TrainingMod TrainingMod;
        public bool trainingModeStart { get; private set; } = false;
        public static bool LLB_VersionCompatibility
        {
            // Get the version of Lethal League Blaze and check if greater than v1.23
            get
            {
                float f;
                if (float.TryParse(JPLELOFJOOH.MDGMBEMFDJK, out f))
                {
                    return f < 1.23f;
                }
                else
                {
                    return false;
                }
            }
        }

#if DEBUG
        private ATDebug atDebug = null;
#endif
        public float cooldownTimer = 0;

        #region Keycode Variables
        public ConfigEntry<bool> ShowCharacterInfo { get; private set; }
        public ConfigEntry<bool> ShowSmallInfo { get; private set; }
        private ConfigEntry<KeyCode> ModifierKey { get; set; }
        public ConfigEntry<KeyCode> angleDrawerKey { get; private set; }
        public ConfigEntry<KeyCode> HitboxKey { get; private set; }
        public ConfigEntry<KeyCode> FastFallIndicatorKey { get; private set; }
        public ConfigEntry<KeyCode> PartnerSelectKey { get; private set; }
        public ConfigEntry<KeyCode> PauseTimeKey { get; private set; }
        public ConfigEntry<KeyCode> AdvanceFrame { get; private set; }
        public ConfigEntry<KeyCode> SaveStateKey { get; private set; }
        public ConfigEntry<KeyCode> LoadStateKey { get; private set; }
        public ConfigEntry<KeyCode> QuickRestart { get; private set; }
        public ConfigEntry<KeyCode> SwitchToOtherPlayer { get; private set; }
        #endregion

        // Everything from this point until the end of the Initialize function is needed for the injection to go properly. Always include this in your mod.
        public static AdvancedTraining Instance { get; private set; } = null;
        public static ManualLogSource Log { get; private set; } = null;
        public static bool InGame => World.instance != null && (DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.CDOFDJMLGLO || DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.LGILIJKMKOD) && !LLScreen.UIScreen.loadingScreenActive;
        public static bool IsOnline => JOMBNFKIHIC.GDNFJCCCKDM; // GameSettings.isOnline

        private void Awake()
        {
            Instance = this;
            Log = this.Logger;

            InitConfig();
            ATStyle.Init();
        }

        void Start()
        {
            // old mod menu code
            /*
            if (MMI == null) { MMI = gameObject.AddComponent<ModMenuIntegration>(); Logger.LogDebug("AdvancedTraining: Added GameObject \"ModMenuIntegration\""); }
            */
            ModDependenciesUtils.RegisterToModMenu(this.Info);
            Logger.LogInfo("AdvancedTraining Started");
        }

        // New mod menu code (LLBML + Bep Configuration)
        void InitConfig()
        {
            this.ModifierKey = Config.Bind<KeyCode>("Toggles", "modifierKey", KeyCode.Q, "Modifier Key");
            this.angleDrawerKey = Config.Bind<KeyCode>("Toggles", "angleDrawerKey", KeyCode.N, "Angle Drawer");
            this.HitboxKey = Config.Bind<KeyCode>("Toggles", "hitboxMod", KeyCode.M, "Show hitboxes");
            this.FastFallIndicatorKey = Config.Bind<KeyCode>("Toggles", "fastFallIndicator", KeyCode.F, "Show fast falls");
            this.PartnerSelectKey = Config.Bind<KeyCode>("Toggles", "enablePartnerSelectInTraining", KeyCode.F4,
                "Enable Partner Select");
            this.PauseTimeKey = Config.Bind<KeyCode>("Toggles", "PauseTime", KeyCode.R, "Pause time");
            this.AdvanceFrame = Config.Bind<KeyCode>("Toggles", "AdvanceFrame", KeyCode.T, "Advance frame");
            this.SaveStateKey = Config.Bind<KeyCode>("Toggles", "SaveState", KeyCode.F2, "Save State");
            this.LoadStateKey = Config.Bind<KeyCode>("Toggles", "LoadState", KeyCode.F3, "Load State");
            this.QuickRestart = Config.Bind<KeyCode>("Toggles", "quickRestart", KeyCode.F5, "Quick restart");
            this.SwitchToOtherPlayer = Config.Bind<KeyCode>("Toggles", "switchToTrainingPartner", KeyCode.F6,
                "Switch to training partner");
            this.ShowSmallInfo = Config.Bind<bool>("Toggles", "enableInfoHint", true, "Show info hint");

            this.ShowCharacterInfo = Config.Bind<bool>("Toggles", "enableCharacterInfo", false, "Show character info");
        }
        public string modifierKeyName { get; private set; }
        public bool ModifierKeyAct()
        {
            if (Rewired.ReInput.controllers.GetLastActiveController().ImplementsTemplate<Rewired.IGamepadTemplate>())
            {
                Rewired.IGamepadTemplate gamepad = Rewired.ReInput.controllers.GetLastActiveController().GetTemplate<Rewired.IGamepadTemplate>();
                modifierKeyName = gamepad.center1.source.target.descriptiveName;
                return gamepad.center1.value;
            }
            else
            {
                modifierKeyName = ModifierKey.Value.ToString();
                return Input.GetKey(ModifierKey.Value);
            }
        }

        public bool ModifierKeyDownAct()
        {
            if (Rewired.ReInput.controllers.GetLastActiveController().ImplementsTemplate<Rewired.IGamepadTemplate>())
            {
                Rewired.IGamepadTemplate gamepad = Rewired.ReInput.controllers.GetLastActiveController().GetTemplate<Rewired.IGamepadTemplate>();
                modifierKeyName = gamepad.center1.source.target.descriptiveName;
                return gamepad.center1.justPressed;
            }
            else
            {
                modifierKeyName = ModifierKey.Value.ToString();
                return Input.GetKeyDown(ModifierKey.Value);
            }
        }

#if DEBUG

        void PrintAllGameObjects()
        {
            string txt = "";
            foreach (var name in FindObjectsOfType<GameObject>())
            {
                string str = (name.transform.parent != null) ? name.transform.parent.gameObject.name : "NO_PARENT";
                string index = (name.transform.parent != null) ? $": {name.transform.GetSiblingIndex()}" : "";
                txt += $"{str}/{name.name} {index}\n";
            }
            Logger.LogDebug(txt);
        }

        void PrintComponetns(string gameObjectName)
        {
            if (gameObjectName == "")
            {
                return;
            }
            GameObject gameO = GameObject.Find(gameObjectName);
            string txt = "";
            foreach (var comp in gameO.GetComponents(typeof(Component)))
            {
                txt += $"{comp}\n";
            }
            Logger.LogDebug(txt);
        }

        void PrintComponetns(GameObject gameObject)
        {
            if (gameObject == null)
            {
                return;
            }
            string txt = "";
            foreach (var comp in gameObject.GetComponents(typeof(Component)))
            {
                txt += $"{comp}\n";
            }
            Logger.LogDebug(txt);
        }

        void BtBallTypeClick(int d = 1)
        {
            BallType ballType = JOMBNFKIHIC.GIGAKBJGFDI.HKOFJKJDPGL;
            int num = (int)ballType;
            if (JOMBNFKIHIC.GIGAKBJGFDI.HKOFJKJDPGL == BallType.GRAVITY)
            {
                num = 0;
            }
            else
            {
                if (ballType == BallType.BEACH) { num++; }
                num++;
            }
            DNPFJHMAIBP.GKBNNFEAJGO(Msg.SEL_SETTING, (int)GameSetting.BALL_TYPE, num);
        }
#endif
        GameObject obOption;
        IADGOHHCMPA ruleWait;
        void Update()
        {
            // old mod menu code
            //ModMenuInit();

            //Debug Code
#if DEBUG
            /*World.instance.stageMin = new IBGCBLLKIHA(ConvertTo.Floatf(-6.66f),ConvertTo.Floatf(0));
            World.instance.stageMax = new IBGCBLLKIHA(ConvertTo.Floatf(6.66f),ConvertTo.Floatf(4.96f));
            World.instance.stageSize = new IBGCBLLKIHA(ConvertTo.Floatf(13.32f),ConvertTo.Floatf(4.96f));*/

            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                if (atDebug == null)
                {
                    PrintAllGameObjects();
                    /*
                    FindObjectOfType<ScreenPlayersSettings>().btBallType.SetEnabled(true);
                    FindObjectOfType<ScreenPlayersSettings>().btBallType.gameObject.SetActive(true);
                    FindObjectOfType<ScreenPlayersSettings>().btBallType.onClick = delegate (int playerNr)
                    {
                        BtBallTypeClick(1);
                    };*/


                    //string objectName = "Pool";
                    //GameObject boomImage = GameObject.Find(objectName).transform.GetChild(0).gameObject;
                    //Logger.LogDebug($"{boomImage.name}");
                    //PrintComponetns(boomImage);

                    atDebug = gameObject.AddComponent<ATDebug>();
                }
                else
                {
                    Destroy(atDebug);
                }
                foreach (var screen in LLScreen.UIScreen.currentScreens)
                {
                    if (screen == null) { continue; }
                    Logger.LogDebug($"{screen.screenType} : {screen.layer}");
                }
            }
#endif

            if (IsOnline || LLB_VersionCompatibility) { return; }
            if ((CurrentGameMode == GameMode.TRAINING) && InGame || CurrentGameState == JOFJHDJHJGI.ABGFICGIPAD && IsOnline == false)
            {
                if (trainingModeStart == false)
                {
                    trainingModeStart = true;
                    TrainingModStart();
                }
            }
            else
            {
                if (trainingModeStart == true)
                {
                    trainingModeStart = false;
                    TrainingModeEnd();
                }
            }

            if (Input.GetKeyDown(KeyCode.Tab) && InGame)
            {
                Cursor.visible = !Cursor.visible;
            }

            if (Input.GetKeyDown(PartnerSelectKey.Value))
            {
                OnPartnerSelectKeyDown?.Invoke(InGame);
            }

            if (Input.GetKeyDown(HitboxKey.Value))
            {
                OnHitboxKeyDown?.Invoke(InGame);
            }

            if (Input.GetKeyDown(FastFallIndicatorKey.Value))
            {
                OnFastFallIndicatorKeyDown?.Invoke(InGame);
            }

            if (Input.GetKeyDown(angleDrawerKey.Value))
            {
                OnAngleDrawerKeyDown?.Invoke(InGame);
            }

            playerMain = ALDOKEMAOMB.BJDPHEHJJJK(0); // Player.Get(0);
            playerEntityMain = playerMain.JCCIAMJEODH;  // PlayerEntity.Player

        }

        protected virtual void TrainingModStart()
        {
            TrainingMod = Instance.gameObject.AddComponent<TrainingMod>();
            OnTrainingModeStart?.Invoke();
        }
        protected virtual void TrainingModeEnd()
        {
            OnTrainingModeEnd?.Invoke();
            TrainingMod = null;
        }

        public const float DESIGN_WIDTH = 1920;
        public const float DESIGN_HEIGHT = 1080;


        void OnGUI()
        {
            ConvertTo.ResolutionScale();
            if (LLB_VersionCompatibility)
            {
                GUIStyle label = new GUIStyle(GUI.skin.box)
                {
                    fontSize = 14,
                    alignment = TextAnchor.MiddleLeft,
                    wordWrap = true,
                };
                int height = 60;
                GUI.Label(new Rect(0, 0, 390, height), $"Incompatible LLB Version \"v{JPLELOFJOOH.MDGMBEMFDJK}\" Detected.\nMUST BE v1.23 or higher", label);
                return;
            }
        }

        public int moveBallSpeed = 21;

#if DEBUG
        public List<TargetEntity> targets;
        public int maxTargets = 6;
        public void SpawnTargets()
        {
            targets = new List<TargetEntity>(maxTargets);
            for (int i = 0; i < maxTargets; i++)
            {
                TargetEntity targetEntity = new GameObject().AddComponent<TargetEntity>();
                targetEntity.Init(new AnimatableData(1));
                HHBCPNCDNDH cgjjehppoan = HHBCPNCDNDH.FCGOICMIBEA(World.instance.stageSize.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2));
                IBGCBLLKIHA position = IBGCBLLKIHA.DBOMOJGKIFI;
                if (i == 0)
                {
                    //position = new IBGCBLLKIHA(HHBCPNCDNDH.FCKBPDNEAOG(HHBCPNCDNDH.NKKIFJJEPOL(9), cgjjehppoan), HHBCPNCDNDH.NKKIFJJEPOL(4.8m));
                    position = ConvertTo.Vector2f(new Vector2(Random.Range(ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN), ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN) / 2), 4));
                }
                if (i == 1)
                {
                    //position = new IBGCBLLKIHA(HHBCPNCDNDH.FCKBPDNEAOG(HHBCPNCDNDH.NKKIFJJEPOL(1), cgjjehppoan), HHBCPNCDNDH.NKKIFJJEPOL(3.2m));
                    position = ConvertTo.Vector2f(new Vector2(Random.Range(ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN), ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN) / 2), 3));
                }
                if (i == 2)
                {
                    //position = new IBGCBLLKIHA(HHBCPNCDNDH.FCKBPDNEAOG(HHBCPNCDNDH.NKKIFJJEPOL(3.5m), cgjjehppoan), HHBCPNCDNDH.NKKIFJJEPOL(4.8m));
                    position = ConvertTo.Vector2f(new Vector2(Random.Range(ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN) / 2, 0), 2));
                }
                if (i == 3)
                {
                    //position = new IBGCBLLKIHA(HHBCPNCDNDH.FCKBPDNEAOG(HHBCPNCDNDH.NKKIFJJEPOL(6), cgjjehppoan), HHBCPNCDNDH.NKKIFJJEPOL(4.2m));
                    position = ConvertTo.Vector2f(new Vector2(Random.Range(ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN) / 2, 0), 3));
                }
                if (i == 4)
                {
                    //position = new IBGCBLLKIHA(HHBCPNCDNDH.FCKBPDNEAOG(HHBCPNCDNDH.NKKIFJJEPOL(10), cgjjehppoan), HHBCPNCDNDH.NKKIFJJEPOL(3.2m));
                    position = ConvertTo.Vector2f(new Vector2(Random.Range(0, ConvertTo.Float(World.instance.stageMax.GCPKPHMKLBN) / 2), 4));
                }
                if (i == 5)
                {
                    //position = new IBGCBLLKIHA(HHBCPNCDNDH.FCKBPDNEAOG(HHBCPNCDNDH.NKKIFJJEPOL(10), cgjjehppoan), HHBCPNCDNDH.NKKIFJJEPOL(3.2m));
                    position = ConvertTo.Vector2f(new Vector2(Random.Range(ConvertTo.Float(World.instance.stageMax.GCPKPHMKLBN) / 2, ConvertTo.Float(World.instance.stageMax.GCPKPHMKLBN)), 2));
                }
                targetEntity.SetPosition(position);
                targetEntity.UpdateUnityTransform();
                targets.Add(targetEntity);
                World.instance.AddEntity(targetEntity);
            }
        }
        public void SpawnTarget()
        {
            targets = new List<TargetEntity>(maxTargets);
            for (int i = 0; i < maxTargets; i++)
            {
                TargetEntity targetEntity = new GameObject().AddComponent<TargetEntity>();
                targetEntity.Init(new AnimatableData(1));
                HHBCPNCDNDH cgjjehppoan = HHBCPNCDNDH.FCGOICMIBEA(World.instance.stageSize.GCPKPHMKLBN, HHBCPNCDNDH.NKKIFJJEPOL(2));
                IBGCBLLKIHA position = IBGCBLLKIHA.DBOMOJGKIFI;
                position = ConvertTo.Vector2f(new Vector2(Random.Range(ConvertTo.Float(World.instance.stageMin.GCPKPHMKLBN), ConvertTo.Float(World.instance.stageMax.GCPKPHMKLBN)), Random.Range(ConvertTo.Float(World.instance.stageMin.CGJJEHPPOAN), ConvertTo.Float(World.instance.stageMax.CGJJEHPPOAN))));
                targetEntity.SetPosition(position);
                targetEntity.UpdateUnityTransform();
                targets.Add(targetEntity);
                World.instance.AddEntity(targetEntity);
            }
        }
#endif

    }
}
