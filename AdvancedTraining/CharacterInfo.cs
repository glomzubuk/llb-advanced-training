﻿using Abilities;
using GameplayEntities;
using LLHandlers;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace AdvancedTraining
{
    class CharacterInfo : MonoBehaviour
    {

        void OnGUI()
        {
            ConvertTo.ResolutionScale();
            if (AdvancedTraining.InGame && AdvancedTraining.IsOnline == false)
            {
                charaRect = ConvertTo.ClampWindowScaled(charaRect);
                if (AdvancedTraining.Instance.ShowCharacterInfo.Value && (!hideCharacterInfo || DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.LGILIJKMKOD))
                {
                    charaRect = GUI.Window(34672851, charaRect, new GUI.WindowFunction(CharaInfo), GUIContent.none, ATStyle.BlueWindowStyle);
                }
            }
        }

        Rect charaRect = new Rect(Screen.width - 350, Screen.height - 453, 350, 453);
        Vector2 mainScrollView;
        List<string> currentAbilityList = new List<string>();
        readonly bool[] showMovement = new bool[5];
        bool showAbilityState;
        bool showCurrentAbility;
        bool showOptions;
        bool showAngles;
        bool hideCharacterInfo;
        bool showHurtbox;
        bool showHitbox;
        bool showItems;
        readonly bool[] showBallData = new bool[5];
        bool[] showPlayerData = new bool[10];
        bool showSaveStates;
        bool showAsPixel = true;
        bool showEZAngle = true;
        int playerInfoIndex = 0;

        const int MAX_SAVES = 3;
        WorldData[] worldDatasSaves = new WorldData[MAX_SAVES];
        int[] frameNrSaves = new int[MAX_SAVES];

        public void ATSaveState(int i)
        {
            if (worldDatasSaves[i] == null)
            {
                worldDatasSaves[i] = new WorldData();
            }
            worldDatasSaves[i].NDEEDGCMMOC(World.instance.worldData, true);
            frameNrSaves[i] = OGONAGCFDPK.DDBJKEIHELD;
        }

        public void ATLoadState(int i)
        {
            if (worldDatasSaves[i] != null)
            {
                World.instance.worldData.NDEEDGCMMOC(worldDatasSaves[i], false);
            }
            LLScreen.ScreenGameHud.UpdateSpeed(BallHandler.instance.GetBall(0).ballData.flySpeed);
            OGONAGCFDPK.HIMHHFDMOEJ(frameNrSaves[i]);
            World.instance.LoadedState();
            World.instance.FrameUpdate();
        }

        void CharaInfo(int wId)
        {
            try
            {
                PlayerEntity charaInfoPlayer = ALDOKEMAOMB.BJDPHEHJJJK(playerInfoIndex).JCCIAMJEODH;

                SkatePlayer skate = charaInfoPlayer as SkatePlayer;
                BossPlayer boss = charaInfoPlayer as BossPlayer;
                BagPlayer bag = charaInfoPlayer as BagPlayer;
                CrocPlayer croc = charaInfoPlayer as CrocPlayer;
                CopPlayer cop = charaInfoPlayer as CopPlayer;
                List<string> tempCurAbilityList = new List<string>();
                List<string> abilityList = new List<string>();
                List<string> abilityStateList = new List<string>();

                if (charaInfoPlayer.GetCurrentAbility() != null && showCurrentAbility)
                {
                    string abilState = charaInfoPlayer.moveableData.abilityState;
                    foreach (string name in charaInfoPlayer.GetCurrentAbility().abilityStatesSequence)
                    {
                        if (abilState == charaInfoPlayer.GetAbilityState(name).name)
                        {
                            tempCurAbilityList.Add(string.Format($"<b>{charaInfoPlayer.GetAbilityState(name).name}</b> : <b>{Mathf.Ceil(ConvertTo.Time60Frames(charaInfoPlayer.GetAbilityState(name).duration))}</b>"));
                        }
                        else
                        {
                            tempCurAbilityList.Add(string.Format($"{charaInfoPlayer.GetAbilityState(name).name} : <b>{Mathf.Ceil(ConvertTo.Time60Frames(charaInfoPlayer.GetAbilityState(name).duration))}</b>"));
                        }
                    }
                    if (currentAbilityList != tempCurAbilityList)
                    {
                        currentAbilityList = tempCurAbilityList;
                    }
                }

                if (showAbilityState)
                {
                    foreach (KeyValuePair<string, Ability> keyValuePair in charaInfoPlayer.abilities)
                    {
                        abilityList.Add($"\n<b>―――― {keyValuePair.Key} ――――</b>");
                        foreach (string name in charaInfoPlayer.GetAbility(keyValuePair.Key).abilityStatesSequence)
                        {
                            abilityList.Add($"{charaInfoPlayer.GetAbilityState(name).name} : <b>{Mathf.Ceil(ConvertTo.Time60Frames(charaInfoPlayer.GetAbilityState(name).duration))}</b>");
                        }
                    }
                }

                if (showAbilityState)
                {
                    foreach (KeyValuePair<string, AbilityState> keyValuePair2 in charaInfoPlayer.abilityStates)
                    {
                        if (ConvertTo.Float(charaInfoPlayer.GetAbilityState(keyValuePair2.Key).duration) != 0f)
                        {
                            abilityStateList.Add(string.Concat(new string[]
                            {
                            string.Format($"{charaInfoPlayer.GetAbilityState(keyValuePair2.Key).name} : <b>{Mathf.Ceil(ConvertTo.Time60Frames(charaInfoPlayer.GetAbilityState(keyValuePair2.Key).duration))}</b>"),
                            }));
                            foreach (string item in abilityList)
                            {
                                if (abilityStateList.Contains(item))
                                {
                                    abilityStateList.Remove(item);
                                }
                            }
                        }
                    }
                }

                List<string> hurtBoxList = new List<string>();
                foreach (KeyValuePair<string, PlayerBox> playerBox in charaInfoPlayer.hurtboxes)
                {
                    IBGCBLLKIHA hubSize = playerBox.Value.bounds.IACOKHPMNGN;
                    if (showAsPixel)
                    {
                        hurtBoxList.Add($"{playerBox.Key} : <b>{ConvertTo.PixelStandard(hubSize.GCPKPHMKLBN)}, {ConvertTo.PixelStandard(hubSize.CGJJEHPPOAN)}</b>");
                    }
                    else
                    {
                        hurtBoxList.Add($"{playerBox.Key} : <b>{ConvertTo.BUnits(hubSize.GCPKPHMKLBN)}, {ConvertTo.BUnits(hubSize.CGJJEHPPOAN)}</b>");
                    }
                }

                List<string> hitBoxList = new List<string>();
                foreach (KeyValuePair<string, PlayerHitbox> playerHitBox in charaInfoPlayer.hitboxes)
                {
                    IBGCBLLKIHA hubSize = playerHitBox.Value.bounds.IACOKHPMNGN;
                    if (showAsPixel)
                    {
                        hitBoxList.Add($"{playerHitBox.Key} : <b>{ConvertTo.PixelStandard(hubSize.GCPKPHMKLBN)}, {ConvertTo.PixelStandard(hubSize.CGJJEHPPOAN)}</b>");
                    }
                    else
                    {
                        hitBoxList.Add($"{playerHitBox.Key} : <b>{ConvertTo.BUnits(hubSize.GCPKPHMKLBN)}, {ConvertTo.BUnits(hubSize.CGJJEHPPOAN)}</b>");
                    }
                }

                GUIStyle abilityStyle = new GUIStyle()
                {
                    fixedHeight = 120,
                };

                GUIStyle border = new GUIStyle()
                {
                    padding = new RectOffset(10, 0, 24, 10),
                };

                GUILayout.Label("Character Info", ATStyle.HeaderStyle);
                mainScrollView = GUILayout.BeginScrollView(mainScrollView, false, true, new GUIStyle(GUI.skin.horizontalScrollbar), new GUIStyle(GUI.skin.verticalScrollbar));
                GUILayout.BeginVertical(ATStyle.HeaderScrollAreaStyle);

                GUI.SetNextControlName("Current Ability");
                showCurrentAbility = GUILayout.Toggle(showCurrentAbility, "Current Ability", ATStyle.LabdivStyle);
                if (showCurrentAbility)
                {
                    GUILayout.BeginVertical(abilityStyle);
                    foreach (string name in currentAbilityList)
                    {
                        GUILayout.Label(name, ATStyle.LabStyleLeft);
                    }
                    GUILayout.EndVertical();
                }

                GUI.SetNextControlName("Player Info");
                if (showPlayerData[0] = GUILayout.Toggle(showPlayerData[0], "Player Info", ATStyle.LabdivStyle))
                {
                    playerInfoIndex = (int)Mathf.Round(GUILayout.HorizontalSlider(playerInfoIndex, 0, PlayerHandler.instance.PlayersInPlay() - 1, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    GUILayout.Label($"Heading : <b>{charaInfoPlayer.entityData.heading}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Player State : <b>{charaInfoPlayer.moveableData.playerState}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Throw Height : <b>{charaInfoPlayer.pxHeight / 2 + ConvertTo.PixelStandard(charaInfoPlayer.throwOffset.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);
                    if (showPlayerData[1] = GUILayout.Toggle(showPlayerData[1], "Ability Data", ATStyle.SplitterStyle))
                    {
                        string ability = charaInfoPlayer.GetCurrentAbility() != null ? charaInfoPlayer.GetCurrentAbility().name : "";
                        GUILayout.Label($"Ability : <b>{ability}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ability State : <b>{charaInfoPlayer.moveableData.abilityState}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ability State Timer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.moveableData.abilityStateTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ability State Duration : <b>{Mathf.Ceil(ConvertTo.Time60Frames(charaInfoPlayer.GetAbilityState(charaInfoPlayer.moveableData.abilityState).duration))}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[2] = GUILayout.Toggle(showPlayerData[2], "Hitstun Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Hitpause State : <b>{charaInfoPlayer.attackingData.hitPauseState}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Hitstun Timer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.hitableData.hitstunTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Hitstun Duration : <b>{ConvertTo.Time60Frames(charaInfoPlayer.hitableData.hitstunDuration)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Grab Guard Timer: <b>{ConvertTo.Time60Frames(charaInfoPlayer.abilityData.grabGuardTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Parry Active Timer: <b>{ConvertTo.Time60Frames(charaInfoPlayer.abilityData.parryActiveTimer)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[3] = GUILayout.Toggle(showPlayerData[3], "Charge Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Charge Power : <b>{ConvertTo.Time60Frames(charaInfoPlayer.playerData.chargePower)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Charge Max Duration : <b>{ConvertTo.Time60Frames(charaInfoPlayer.chargeMaxDuration)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Charge Speed Additional: <b>{ConvertTo.Int(ConvertTo.Multiply(ConvertTo.Divide(charaInfoPlayer.attackingData.chargePower, charaInfoPlayer.chargeMaxDuration), HHBCPNCDNDH.NKKIFJJEPOL(10.0m)))}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Full Charge Margin : <b>{ConvertTo.Time60Frames(charaInfoPlayer.fullChargeMargin)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[4] = GUILayout.Toggle(showPlayerData[4], "Big Hit Data", ATStyle.SplitterStyle))
                    {
                        HHBCPNCDNDH bigHitMulti = ConvertTo.Max(ConvertTo.Add(charaInfoPlayer.playerData.bigHitsCountAdditional, ConvertTo.Floatf(1)), ConvertTo.Floatf(1.1f));
                        GUILayout.Label($"Big Hits : <b>{charaInfoPlayer.playerData.bigHitsCount}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Big Hit Multiplier : <b>x{ConvertTo.Float(bigHitMulti)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Big Hit +Speed : <b>{ConvertTo.Pixels(ConvertTo.Subtract(ConvertTo.Multiply(BallHandler.instance.GetBall(0).GetFlySpeed(true), bigHitMulti), BallHandler.instance.GetBall(0).ballData.flySpeed), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"FlySpeed on BigHit : <b>{ConvertTo.Pixels(ConvertTo.Multiply(BallHandler.instance.GetBall(0).GetFlySpeed(true), bigHitMulti), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[5] = GUILayout.Toggle(showPlayerData[5], "Buffer Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Parry Buffered : <b>{charaInfoPlayer.playerData.bufferedParry}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Special Buffered : <b>{charaInfoPlayer.playerData.bufferedSpecial}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ability Buffered : <b>{charaInfoPlayer.playerData.bufferAbility}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[6] = GUILayout.Toggle(showPlayerData[6], "Entity Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Active : <b>{charaInfoPlayer.entityData.active}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Heading : <b>{charaInfoPlayer.entityData.heading}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"On Ground : <b>{charaInfoPlayer.entityData.onGround}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"On Right Wall Approx : <b>{charaInfoPlayer.entityData.onRightWallApprox}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"On Right Wall : <b>{charaInfoPlayer.entityData.onRightWall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"On Left Wall Approx : <b>{charaInfoPlayer.entityData.onLeftWallApprox}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"On Left Wall : <b>{charaInfoPlayer.entityData.onLeftWall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"On Ceiling Approx : <b>{charaInfoPlayer.entityData.onCeilingApprox}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[7] = GUILayout.Toggle(showPlayerData[7], "Attacking Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Hit Pause State : <b>{charaInfoPlayer.attackingData.hitPauseState}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Energy : <b>{charaInfoPlayer.attackingData.energy}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Counter Parry Active : <b>{charaInfoPlayer.attackingData.counterParryActive}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Countered Amount : <b>{charaInfoPlayer.attackingData.counteredAmount}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"parryHeld : <b>{charaInfoPlayer.attackingData.parryHeld}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"allowParry : <b>{charaInfoPlayer.attackingData.allowParry}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"hitSomething : <b>{charaInfoPlayer.attackingData.hitSomething}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"parryKnockbackDuration : <b>{ConvertTo.Time60Frames(charaInfoPlayer.attackingData.parryKnockbackDuration)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"ParryPos : <b>({ConvertTo.Float(charaInfoPlayer.attackingData.parryPos.GCPKPHMKLBN)}, {ConvertTo.Float(charaInfoPlayer.attackingData.parryPos.GCPKPHMKLBN)})</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"ParrySuccess : <b>{charaInfoPlayer.attackingData.parrySuccess}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"inParryExtendedHitpause : <b>{charaInfoPlayer.attackingData.inParryExtendedHitpause}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Parry Active Timer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.attackingData.parryActiveTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Grab Guard Timer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.attackingData.grabGuardTimer)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[8] = GUILayout.Toggle(showPlayerData[8], "Animation Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Current Animation : <b>{charaInfoPlayer.GetAnimDataOfVisual("main").currentAnim}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Animation Time: <b>{ConvertTo.Float(ConvertTo.Multiply(charaInfoPlayer.GetAnimDataOfVisual("main").animTime, 12))}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"At Animation End: <b>{charaInfoPlayer.GetAnimDataOfVisual("main").atAnimEnd}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showPlayerData[9] = GUILayout.Toggle(showPlayerData[9], "Ability Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"recoveryTimer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.abilityData.recoveryTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"inGetUpBlaze : <b>{charaInfoPlayer.abilityData.inGetUpBlaze}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"checkedActionsThisFrame : <b>{charaInfoPlayer.abilityData.checkedActionsThisFrame}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"preHeading : <b>{charaInfoPlayer.abilityData.preHeading}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"specialHeading : <b>{charaInfoPlayer.abilityData.specialHeading}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"specialAmount : <b>{charaInfoPlayer.abilityData.specialAmount}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"specialFAmount : <b>{ConvertTo.Float(charaInfoPlayer.abilityData.specialFAmount)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"specialVector : <b>{ConvertTo.Float(charaInfoPlayer.abilityData.specialVector.GCPKPHMKLBN)}, {ConvertTo.Float(charaInfoPlayer.abilityData.specialVector.GCPKPHMKLBN)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"specialVector2 : <b>{ConvertTo.Float(charaInfoPlayer.abilityData.specialVector2.GCPKPHMKLBN)}, {ConvertTo.Float(charaInfoPlayer.abilityData.specialVector2.GCPKPHMKLBN)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"specialBool : <b>{charaInfoPlayer.abilityData.specialBool}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"reUseWallTimer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.abilityData.reUseWallTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"isEatingStuff : <b>{charaInfoPlayer.abilityData.isEatingStuff}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"eatBallTimer : <b>{ConvertTo.Time60Frames(charaInfoPlayer.abilityData.eatBallTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"eatBallMoveDuration : <b>{ConvertTo.Time60Frames(charaInfoPlayer.abilityData.eatBallMoveDuration)}</b>", ATStyle.LabStyleLeft);

                    }
                }

                GUI.SetNextControlName("Hurt Boxes");
                if (showHurtbox = GUILayout.Toggle(showHurtbox, "Hurt Boxes", ATStyle.LabdivStyle))
                {
                    foreach (string name in hurtBoxList)
                    {
                        GUILayout.Label(name, ATStyle.LabStyleLeft);
                    }
                }

                GUI.SetNextControlName("Hit Boxes");
                if (showHitbox = GUILayout.Toggle(showHitbox, "Hit Boxes", ATStyle.LabdivStyle))
                {
                    foreach (string name in hitBoxList)
                    {
                        GUILayout.Label(name, ATStyle.LabStyleLeft);
                    }
                }

                GUI.SetNextControlName("Angles");
                if (showAngles = GUILayout.Toggle(showAngles, "Angles", ATStyle.LabdivStyle))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine($"Up : <b>{ConvertTo.EzAngle(charaInfoPlayer.hitAngleUp, showEZAngle)}</b>");
                    sb.AppendLine($"Down : <b>{ConvertTo.EzAngle(charaInfoPlayer.hitAngleDown, showEZAngle)}</b>");
                    if (charaInfoPlayer.hitAngleDown != charaInfoPlayer.hitAngleNeutralDownAir) sb.AppendLine($"Air Down : <b>{ConvertTo.EzAngle(charaInfoPlayer.hitAngleNeutralDownAir, showEZAngle)}</b>");
                    sb.AppendLine($"Smash : <b>{ConvertTo.EzAngle(charaInfoPlayer.hitAngleSmash, showEZAngle)}</b>");
                    sb.AppendLine($"Spike-F : <b>{ConvertTo.EzAngle(charaInfoPlayer.hitAngleDownAirForward, showEZAngle)}</b>");
                    sb.AppendLine($"Spike-B : <b>{ConvertTo.EzAngle(charaInfoPlayer.hitAngleDownAirBackward, showEZAngle)}</b>");
                    if (croc != null) sb.AppendLine($"Wall Swing Down : <b>{ConvertTo.EzAngle(ConvertTo.Int(croc.wallClimbDownAim), showEZAngle)}</b>");
                    if (croc != null) sb.AppendLine($"Spit Down-Forward : <b>{ConvertTo.EzAngle(18, showEZAngle)}</b>");
                    if (cop != null) sb.AppendLine($"Cuff Down : <b>{ConvertTo.EzAngle(ConvertTo.Int(cop.cuffAngleDown), showEZAngle)}</b>");
                    if (charaInfoPlayer.character == Character.BOOM) sb.AppendLine($"The Beat : <b>{ConvertTo.EzAngle(315, showEZAngle)}</b>");
                    if (charaInfoPlayer.character == Character.PONG) sb.AppendLine($"Ground Spin : <b>{ConvertTo.EzAngle(352, showEZAngle)}</b>");
                    if (charaInfoPlayer.character == Character.PONG) sb.AppendLine($"Ceiling Spin : <b>{ConvertTo.EzAngle(45, showEZAngle)}</b>");
                    GUILayout.Label(sb.ToString(), ATStyle.LabStyleLeft);
                }

                GUI.SetNextControlName("Movement");
                if (showMovement[0] = GUILayout.Toggle(showMovement[0], "Movement", ATStyle.LabdivStyle))
                {
                    GUILayout.Label($"Velocity-X : <b>{ConvertTo.Pixels(charaInfoPlayer.moveableData.velocity.GCPKPHMKLBN, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Velocity-Y : <b>{ConvertTo.Pixels(charaInfoPlayer.moveableData.velocity.CGJJEHPPOAN, showAsPixel)}</b>", ATStyle.LabStyleLeft);

                    GUILayout.Label($"Position-X : <b>{ConvertTo.Value(charaInfoPlayer.moveableData.prePosition.GCPKPHMKLBN, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Position-Y : <b>{ConvertTo.Value(charaInfoPlayer.moveableData.prePosition.CGJJEHPPOAN, showAsPixel)}</b>", ATStyle.LabStyleLeft);


                    if (showMovement[1] = GUILayout.Toggle(showMovement[1], "Ground Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Ground Initial Speed: <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.maxMove, 0.5f), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ground Accelaration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.groundAcc, World.FDELTA_TIME), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ground Decelaration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.groundDeacc, World.FDELTA_TIME), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ground Top Speed : <b>{ConvertTo.Pixels(ConvertTo.Add(charaInfoPlayer.maxMove, HHBCPNCDNDH.NKKIFJJEPOL(0.3m)), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Slide Decelaration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.slideDeacc, World.FDELTA_TIME), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showMovement[2] = GUILayout.Toggle(showMovement[2], "Air Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Air Accelaration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.airAcc, World.FDELTA_TIME), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Air Decelaration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.airDeacc, World.FDELTA_TIME), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Air Top Speed : <b>{ConvertTo.Pixels(ConvertTo.Add(charaInfoPlayer.maxAirMove, HHBCPNCDNDH.NKKIFJJEPOL(0.3m)), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showMovement[3] = GUILayout.Toggle(showMovement[3], "Fall Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Fall Accelaration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.gravityForce, World.FDELTA_TIME), showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Fall Top Speed : <b>{ConvertTo.Pixels(charaInfoPlayer.maxGravity, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Fast-Fall Speed : <b>{ConvertTo.Pixels(charaInfoPlayer.gravityForceFastFall, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        if (bag != null) GUILayout.Label($"Kite Duration : <b>{ConvertTo.Float(bag.maxKiteFuel)} Sec</b>", ATStyle.LabStyleLeft);
                    }
                    if (showMovement[4] = GUILayout.Toggle(showMovement[4], "Jump Data", ATStyle.SplitterStyle))
                    {
                        StringBuilder sb = new StringBuilder();
                        if (skate != null) sb.AppendLine($"Hover Duration : <b>{ConvertTo.Float(skate.maxFuel)} Sec</b>");
                        if (boss != null) sb.AppendLine($"Fly Duration : <b>{ConvertTo.Float(boss.maxFuel)} Sec</b>");
                        sb.AppendLine($"Jump Deceleration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.gravityForceUp, World.FDELTA_TIME), showAsPixel)}</b>");
                        sb.AppendLine($"Jump Power : <b>{ConvertTo.Pixels(charaInfoPlayer.jumpPower, showAsPixel)}</b>");
                        sb.AppendLine($"Extra Jump Power : <b>{ConvertTo.Pixels(charaInfoPlayer.extraJumpPower, showAsPixel)}</b>");
                        sb.AppendLine($"Extra Jump Amount : <b>{charaInfoPlayer.extraJumpAmount}</b>");
                        if (!ConvertTo.Equal(charaInfoPlayer.highJumpPower, HHBCPNCDNDH.DBOMOJGKIFI)) sb.AppendLine($"Super Jump Power : <b>{ConvertTo.Pixels(charaInfoPlayer.highJumpPower, showAsPixel)}</b>");
                        sb.AppendLine($"Apex Deceleration : <b>{ConvertTo.Pixels(ConvertTo.Multiply(charaInfoPlayer.gravityForceApex, World.FDELTA_TIME), showAsPixel)}</b>");
                        sb.AppendLine($"Apex In : <b>{ConvertTo.Pixels(charaInfoPlayer.apexIn, showAsPixel)}</b>");
                        sb.AppendLine($"Apex Out : <b>{ConvertTo.Pixels(charaInfoPlayer.apexOut, showAsPixel)}</b>");
                        sb.AppendLine($"Jump Height : <b>{ConvertTo.Pixels(CalcJumpHeight(charaInfoPlayer, charaInfoPlayer.jumpPower), showAsPixel)}</b>");
                        sb.AppendLine($"Extra Jump Height : <b>{ConvertTo.Pixels(CalcJumpHeight(charaInfoPlayer, charaInfoPlayer.extraJumpPower), showAsPixel)}</b>");
                        if (!ConvertTo.Equal(charaInfoPlayer.highJumpPower, HHBCPNCDNDH.DBOMOJGKIFI))
                        {
                            sb.AppendLine($"Super Jump Height : <b>{ConvertTo.Pixels(CalcJumpHeight(charaInfoPlayer, charaInfoPlayer.highJumpPower), showAsPixel)}</b>");
                        }
                        GUILayout.Label(sb.ToString(), ATStyle.LabStyleLeft);
                    }
                }

                if (showItems = GUILayout.Toggle(showItems, "Item Data", ATStyle.LabdivStyle))
                {
                    ItemHandler itemHandler = ItemHandler.instance;
                    CorpseEntity[] corpseEntities = itemHandler.corpseItems;
                    for (int i = 0; i < itemHandler.corpseItems.Length; i++)
                    {
                        GUILayout.Label($"{corpseEntities[i].entityName} : {corpseEntities[i].character}", ATStyle.LabStyleLeft);
                        GUILayout.Label($"isActive : {corpseEntities[i].itemData.active}", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Position: {ConvertTo.Value(corpseEntities[i].GetPosition().GCPKPHMKLBN, showAsPixel)} : {ConvertTo.Value(corpseEntities[i].GetPosition().CGJJEHPPOAN, showAsPixel)}", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Visual Name : {corpseEntities[i].GetVisual()?.anim.name}", ATStyle.LabStyleLeft);
                        GUILayout.Label($"isVisible : {corpseEntities[i].GetVisual()?.gameObject.activeSelf}", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Animation : {corpseEntities[i].animatableData?.animData[0].currentAnim} \n", ATStyle.LabStyleLeft);
                    }
                    if (Input.GetKeyDown(KeyCode.Keypad5))
                    {
                        corpseEntities[1].itemData.active = true;
                    }
                }

                if (showBallData[0] = GUILayout.Toggle(showBallData[0], "Ball Data", ATStyle.LabdivStyle))
                {
                    BallEntity ball = BallHandler.instance.GetBall();
                    GUILayout.Label($"Velocity-X : <b>{ConvertTo.Pixels(ball.entityData.velocity.GCPKPHMKLBN, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Velocity-Y : <b>{ConvertTo.Pixels(ball.entityData.velocity.CGJJEHPPOAN, showAsPixel)}</b>", ATStyle.LabStyleLeft);

                    GUILayout.Label($"Position-X : <b>{ConvertTo.Value(ball.entityData.prePosition.GCPKPHMKLBN, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Position-Y : <b>{ConvertTo.Value(ball.entityData.prePosition.CGJJEHPPOAN, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Scale : <b>{ConvertTo.Value(ball.ballData.ballScale.GCPKPHMKLBN, showAsPixel)}</b>, <b>{ConvertTo.Value(ball.ballData.ballScale.CGJJEHPPOAN, showAsPixel)}</b>", ATStyle.LabStyleLeft);

                    if (showBallData[1] = GUILayout.Toggle(showBallData[1], "Fly Direction", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Fly Speed : <b>{ConvertTo.Pixels(ball.hitableData.flySpeed, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Fly Speed Ball: <b>{ConvertTo.Pixels(ball.ballData.flySpeed, showAsPixel)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Fly Direction-X : <b>{ConvertTo.Float(ball.hitableData.flyDirection.GCPKPHMKLBN)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Fly Direction-Y : <b>{ConvertTo.Float(ball.hitableData.flyDirection.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showBallData[2] = GUILayout.Toggle(showBallData[2], "Hitstun Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Hitstun State : <b>{ball.hitableData.hitstunState}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Hitstun Timer : <b>{ConvertTo.Time60Frames(ball.hitableData.hitstunTimer)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Hitstun Duration : <b>{ConvertTo.Time60Frames(ball.hitableData.hitstunDuration)}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showBallData[3] = GUILayout.Toggle(showBallData[3], "Bouncing Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Special Hit Hori Wall : <b>{ball.bouncingData.specialHitHoriWall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Special Hit Vert Wall : <b>{ball.bouncingData.specialHitVertWall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Hit Horizontal Wall : <b>{ball.bouncingData.hitHorizontalWall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"From Down Bunt : <b>{ball.bouncingData.fromDownBunt}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"From Counter : <b>{ball.bouncingData.fromCounter}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"From Grab Deny : <b>{ball.bouncingData.fromGrabDeny}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"From Special : <b>{ball.bouncingData.fromSpecial}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Ball State : <b>{ball.bouncingData.ballState}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Wall Grind Side : <b>{ball.bouncingData.wallGrindSide}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Last Wall : <b>{ball.bouncingData.lastWall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Pong Turn Side : <b>{ball.bouncingData.pongTurnSide}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Pong Aim : <b>{ball.bouncingData.pongAim}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Turn Speed : <b>{ConvertTo.Float(ball.bouncingData.turnSpeed)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Time Since Hit : <b>{ConvertTo.Time60Frames(ball.bouncingData.timeSinceHit)}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Active CuffedBall: <b>{ball.IsActiveCuffedBall()}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Show Direction Amount: <b>{ball.bouncingData.showDirection}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"ShowDirection: <b>{ball.GetShowDirection()}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"isHittingWall: <b>{ball.IsHittingWall()}</b>", ATStyle.LabStyleLeft);
                    }
                    if (showBallData[4] = GUILayout.Toggle(showBallData[4], "Hitable Data", ATStyle.SplitterStyle))
                    {
                        GUILayout.Label($"Can Be Hit By Player : <b>{ball.hitableData.canBeHitByPlayer}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Can Be Hit By Ball : <b>{ball.hitableData.canBeHitByBall}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Can Be Hit By Item : <b>{ball.hitableData.canBeHitByItem}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Can Hit Player : <b>{ball.hitableData.canHitPlayer}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Has Stage Collision : <b>{ball.hitableData.hasStageCollision}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Last Hitter Index : <b>{ball.hitableData.lastHitterIndex}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Last Victim Index : <b>{ball.hitableData.lastVictimIndex}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"From Shortend Hitstun : <b>{ball.hitableData.fromShortendHitstun}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Is Direct : <b>{ball.hitableData.isDirect}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"Team : <b>{ball.hitableData.team}</b>", ATStyle.LabStyleLeft);
                    }
                    if (Input.GetKeyDown(KeyCode.Keypad9))
                    {
                        ball.DontShowDirection();
                    }
                }

                if (showOptions = GUILayout.Toggle(showOptions, "Options", ATStyle.LabdivStyle))
                {
                    GUILayout.Label($"Window Height : <b>{charaRect.height}</b>", ATStyle.LabStyleLeft);
                    charaRect.height = Mathf.Round(GUILayout.HorizontalSlider(charaRect.height, 200, Screen.height, ATStyle.SliderBackgroundStyle, ATStyle.SliderThumbStyle));
                    showAsPixel = GUILayout.Toggle(showAsPixel, showAsPixel ? "Pixel Size" : "B-Unit Size", ATStyle.SplitterStyle);
                    showEZAngle = GUILayout.Toggle(showEZAngle, showEZAngle ? "EZ Angle Values" : "Raw Angle Values", ATStyle.SplitterStyle);
                    hideCharacterInfo = GUILayout.Toggle(hideCharacterInfo, hideCharacterInfo ? "Show Character Info" : "Hide Character Info", ATStyle.SplitterStyle);
                    if (showAsPixel)
                    {
                        GUILayout.Label($"Stage Size : <b>{ConvertTo.PixelStandard(World.instance.stageSize.GCPKPHMKLBN)}, {ConvertTo.PixelStandard(World.instance.stageSize.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);
                    }
                    else
                    {
                        GUILayout.Label($"Stage Size : <b>{ConvertTo.BUnits(World.instance.stageSize.GCPKPHMKLBN)}, {ConvertTo.BUnits(World.instance.stageSize.CGJJEHPPOAN)}</b>", ATStyle.LabStyleLeft);
                    }
#if DEBUG
                    if (Rewired.ReInput.mapping.GetControllerMap(3) != null)
                    {
                        if (Input.GetKeyDown(KeyCode.KeypadEnter))
                        {
                            if (!Rewired.ReInput.mapping.GetControllerMap(3).ContainsAction(32))
                            {
                                Rewired.ReInput.mapping.GetControllerMap(3).CreateElementMap(0, Rewired.Pole.Positive, 12, Rewired.ControllerElementType.Button, Rewired.AxisRange.Full, false, out Rewired.ActionElementMap test);
                            }
                        }
                    }
#endif
                }

                if (showAbilityState = GUILayout.Toggle(showAbilityState, "All Ability States", ATStyle.LabdivStyle))
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (string name in abilityStateList)
                    {
                        sb.AppendLine(name);
                    }
                    foreach (string name in abilityList)
                    {
                        sb.AppendLine(name);
                    }
                    GUILayout.Label(sb.ToString(), ATStyle.LabStyleLeft);
                }

                if (showSaveStates = GUILayout.Toggle(showSaveStates, "Save States", ATStyle.LabdivStyle))
                {
                    GUILayout.Label($"Current Frame : <b>{OGONAGCFDPK.DDBJKEIHELD}</b>", ATStyle.LabStyleLeft);
                    GUILayout.Label($"Frame Timer: <b>{OGONAGCFDPK.DDBJKEIHELD - frameTimer}</b>", ATStyle.LabStyleLeft);
                    if (GUILayout.Button($"Zero Timer", ATStyle.SplitterStyle)) { frameTimer = OGONAGCFDPK.DDBJKEIHELD; }
                    for (int i = 0; i < MAX_SAVES; i++)
                    {
                        GUILayout.BeginHorizontal();
                        if (GUILayout.Button($"Save {i + 1}", ATStyle.ActButton)) ATSaveState(i);
                        if (GUILayout.Button($"Load {i + 1}", ATStyle.DeactButton)) ATLoadState(i);
                        GUILayout.EndHorizontal();
                    }
                    /*if (stateSystem.currentRecordedIns != null)
                    {
                        GUILayout.Label($"Current Inputs : <b>{GetCurrentInput(ALDOKEMAOMB.BJDPHEHJJJK(0))}</b>", ATStyle.LabStyleLeft);
                        int[,] curIn = stateSystem.aTcurInput;
                        GUILayout.Label($"stateSystem Inputs : <b>{curIn[0, 0]}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"frameIncrment : <b>{stateSystem.frameIncrment}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"recordedInput Count : <b>{stateSystem.currentRecordedIns.Count}</b>", ATStyle.LabStyleLeft);
                        GUILayout.Label($"recordedNumber : <b>{recordedNumber}</b>", ATStyle.LabStyleLeft);
                        recordedNumber = (int)Mathf.Round(GUILayout.HorizontalSlider(recordedNumber, 0, stateSystem.currentRecordedIns.Count - 1, ATStyle._sliderBackgroundStyle, ATStyle._sliderThumbStyle));
                        for (int i = 0; i < InputAction.nGameActions; i++)
                        {
                            GUILayout.Label($"Input: {(InputIndex)i} : {stateSystem.currentRecordedIns[recordedNumber][0, i]}");
                        }
                    }*/
                }

                GUILayout.FlexibleSpace();
                GUILayout.EndVertical();
                GUILayout.EndScrollView();
                GUI.DragWindow();
            }
            catch (System.NullReferenceException)
            {

            }
        }
        int frameTimer;

        static HHBCPNCDNDH CalcJumpHeight(PlayerEntity playerEntity, HHBCPNCDNDH power)
        {
            HHBCPNCDNDH jumpHeight = new HHBCPNCDNDH(0);
            for (HHBCPNCDNDH i = power; ConvertTo.LessThan(new HHBCPNCDNDH(0), i); i = ConvertTo.LessThan(i, playerEntity.apexIn) ? ConvertTo.Subtract(i, ConvertTo.Multiply(playerEntity.gravityForceApex, World.FDELTA_TIME)) : ConvertTo.Subtract(i, ConvertTo.Multiply(playerEntity.gravityForceUp, World.FDELTA_TIME)))
            {
                jumpHeight = ConvertTo.Add(i, jumpHeight);
            }
            return jumpHeight;
        }

    }
}
