﻿using System.Collections.Generic;
using UnityEngine;

namespace AdvancedTraining
{
    public static class ATStyle
    {

        private static Dictionary<string, Texture2D> texColor = new Dictionary<string, Texture2D>();

        public static Texture2D GetTexColor(string color)
        {
            return texColor[color];
        }

        public static void Init()
        {
            texColor.Add("Black", ColorToTexture2D(new Color32(0, 0, 0, 255)));
            texColor.Add("White", ColorToTexture2D(new Color32(255, 255, 255, 255)));
            texColor.Add("Red", ColorToTexture2D(new Color32(255, 0, 0, 255)));
            texColor.Add("TeamRed", ColorToTexture2D(new Color32(230, 70, 56, 255)));
            texColor.Add("MintGreen", ColorToTexture2D(new Color32(51, 204, 102, 255)));
            texColor.Add("DarkGreen", ColorToTexture2D(new Color32(0, 51, 0, 255)));
            texColor.Add("LMintGreen", ColorToTexture2D(new Color32(102, 255, 153, 255)));
            texColor.Add("LPink", ColorToTexture2D(new Color32(255, 102, 153, 255)));
            texColor.Add("DarkPink", ColorToTexture2D(new Color32(102, 0, 51, 255)));
            texColor.Add("RedPink", ColorToTexture2D(new Color32(204, 0, 51, 255)));
            texColor.Add("NavyBlue", ColorToTexture2D(new Color32(0, 51, 153, 255)));
            texColor.Add("ATBlue", ColorToTexture2D(new Color32(34, 139, 231, 255)));
            texColor.Add("ATLBlue", ColorToTexture2D(new Color32(102, 204, 255, 255)));
            texColor.Add("SwingOn", ColorToTexture2D(new Color32(48, 182, 206, 255)));
            texColor.Add("SwingOff", ColorToTexture2D(new Color32(18, 68, 77, 255)));
            texColor.Add("BuntOn", ColorToTexture2D(new Color32(230, 73, 107, 255)));
            texColor.Add("BuntOff", ColorToTexture2D(new Color32(77, 24, 36, 255)));
            texColor.Add("GrabOn", ColorToTexture2D(new Color32(228, 220, 78, 255)));
            texColor.Add("GrabOff", ColorToTexture2D(new Color32(77, 74, 26, 255)));
            texColor.Add("JumpOn", ColorToTexture2D(new Color32(109, 194, 96, 255)));
            texColor.Add("JumpOff", ColorToTexture2D(new Color32(43, 77, 37, 255)));
            texColor.Add("TauntOn", ColorToTexture2D(new Color32(150, 95, 173, 255)));
            texColor.Add("TauntOff", ColorToTexture2D(new Color32(66, 42, 76, 255)));
            texColor.Add("DirOn", ColorToTexture2D(new Color32(93, 93, 93, 255)));
            texColor.Add("DirOff", ColorToTexture2D(new Color32(13, 12, 13, 255)));
            texColor.Add("ExpressOn", ColorToTexture2D(new Color32(0, 161, 255, 255)));
            texColor.Add("ExpressOff", ColorToTexture2D(new Color32(0, 48, 76, 255)));
            texColor.Add("MoveBG", ColorToTexture2D(new Color32(13, 12, 13, 255)));
            texColor.Add("BoomDGreen", ColorToTexture2D(new Color32(1, 24, 20, 255)));
            texColor.Add("Clear", ColorToTexture2D(new Color32(0, 0, 0, 0)));
            texColor.Add("Grey50", ColorToTexture2D(new Color32(150, 150, 150, 255)));
            texColor.Add("Grey60", ColorToTexture2D(new Color32(125, 125, 125, 255)));
            texColor.Add("Grey85", ColorToTexture2D(new Color32(54, 54, 54, 255)));
            texColor.Add("Grey90", ColorToTexture2D(new Color32(37, 37, 37, 255)));
        }

        public static GUIStyle BgStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    padding = new RectOffset(4, 4, 4, 4),
                };
                gUIStyle.normal.background = texColor["Black"];
                return gUIStyle;
            }
        }

        public static GUIStyle MainStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    padding = new RectOffset(4, 4, 4, 4),
                };
                gUIStyle.normal.background = texColor["Black"];
                return gUIStyle;
            }
        }

        public static GUIStyle SplitterStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 15,
                    alignment = TextAnchor.MiddleCenter,
                    padding = new RectOffset(4, 4, 4, 4),
                    margin = new RectOffset(4, 4, 4, 4),
                    fixedHeight = 30,
                };

                gUIStyle.normal.background = texColor["Grey50"];
                gUIStyle.normal.textColor = Color.black;

                gUIStyle.hover.background = texColor["Grey60"];
                gUIStyle.onHover.background = texColor["Grey60"];
                return gUIStyle;
            }
        }

        public static GUIStyle ButtonThin
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 15,
                    alignment = TextAnchor.MiddleCenter,
                    padding = new RectOffset(8, 8, 4, 4),
                    margin = new RectOffset(0, 0, 4, 4),
                    fixedHeight = 28,
                };

                gUIStyle.normal.textColor = Color.black;
                gUIStyle.normal.background = texColor["Grey50"];
                gUIStyle.onNormal.background = texColor["MintGreen"];

                gUIStyle.hover.background = texColor["Grey60"];
                gUIStyle.onHover.background = texColor["LMintGreen"];

                gUIStyle.active.background = texColor["Grey60"];
                gUIStyle.onActive.background = texColor["MintGreen"];
                return gUIStyle;
            }
        }

        public static GUIStyle ButtonThinActive
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 15,
                    alignment = TextAnchor.MiddleCenter,
                    padding = new RectOffset(8, 8, 4, 4),
                    margin = new RectOffset(0, 0, 4, 4),
                    fixedHeight = 28,
                };

                gUIStyle.normal.textColor = Color.black;
                gUIStyle.normal.background = texColor["Grey60"];
                gUIStyle.onNormal.background = texColor["Grey90"];

                gUIStyle.hover.background = texColor["Grey50"];
                gUIStyle.onHover.background = texColor["Grey85"];

                gUIStyle.active.background = texColor["Grey60"];
                gUIStyle.onActive.background = texColor["Grey90"];
                return gUIStyle;
            }
        }

        public static GUIStyle ButtonThinExtend
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 15,
                    alignment = TextAnchor.MiddleCenter,
                    padding = new RectOffset(8, 8, 4, 4),
                    margin = new RectOffset(0, 0, 4, 4),
                    fixedHeight = 32,
                };

                gUIStyle.normal.textColor = Color.black;
                gUIStyle.normal.background = texColor["ATBlue"];
                gUIStyle.onNormal.background = texColor["ATBlue"];

                gUIStyle.hover.background = texColor["ATLBlue"]; ;
                gUIStyle.onHover.background = texColor["ATLBlue"];

                gUIStyle.active.background = texColor["ATBlue"];
                gUIStyle.onActive.background = texColor["ATBlue"];
                return gUIStyle;
            }
        }

        public static GUIStyle LabdivStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 17,
                    alignment = TextAnchor.MiddleCenter,
                    padding = new RectOffset(4, 4, 4, 4),
                    margin = new RectOffset(0, 0, 4, 4),
                    fixedHeight = 32,
                };

                gUIStyle.normal.background = texColor["ATBlue"];
                gUIStyle.normal.textColor = Color.white;

                gUIStyle.hover.background = texColor["White"];
                gUIStyle.onHover.background = texColor["White"];
                return gUIStyle;
            }
        }

        public static GUIStyle LabStyleLeft
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    fontSize = 14,
                    alignment = TextAnchor.MiddleLeft,
                    padding = new RectOffset(0, 0, 1, 1)
                };
                gUIStyle.normal.textColor = Color.white;
                return gUIStyle;
            }
        }

        public static GUIStyle BlueWindowStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle();
                gUIStyle.normal.background = texColor["ATBlue"];
                //gUIStyle.onNormal.background = texColor["Red"];;
                return gUIStyle;
            }
        }
        /// <summary>
        /// Area Style. Use when there is no Header
        /// </summary>
        static RectOffset rectOffset = new RectOffset(6, 6, 6, 6);
        public static GUIStyle AreaStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    padding = new RectOffset(6, 6, 6, 6),
                    margin = rectOffset,
                };
                gUIStyle.normal.background = texColor["Black"];
                return gUIStyle;
            }
        }
        /// <summary>
        /// Header Area Style. Use when there is a Header
        /// </summary>
        public static GUIStyle HeaderAreaStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(AreaStyle)
                {
                    margin = new RectOffset(rectOffset.left, rectOffset.right, 0, rectOffset.bottom),
                };
                return gUIStyle;
            }
        }
        public static GUIStyle HeaderScrollAreaStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(AreaStyle)
                {
                    margin = new RectOffset(rectOffset.left, 2, 2, rectOffset.bottom),
                };
                return gUIStyle;
            }
        }
        public static GUIStyle HeaderStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    margin = new RectOffset(0, 0, 3, 3),
                    padding = new RectOffset(0, 0, 0, 0),
                    alignment = TextAnchor.MiddleCenter,
                    fontSize = 14,
                };
                gUIStyle.normal.textColor = Color.black;
                return gUIStyle;
            }
        }

        public static GUIStyle WindStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    padding = new RectOffset(4, 4, 4, 4),
                    alignment = TextAnchor.UpperCenter,
                    fontSize = 14,
                };
                gUIStyle.normal.background = texColor["ATBlue"];
                return gUIStyle;
            }
        }
        public static GUIStyle border = new GUIStyle() { padding = new RectOffset(10, 10, 24, 10), fontSize = 20, };

        public static GUIStyle Button
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.button)
                {
                    fontSize = 24,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter,
                };

                gUIStyle.normal.textColor = Color.white;
                gUIStyle.hover.textColor = Color.white;
                gUIStyle.active.textColor = Color.black;

                gUIStyle.normal.background = texColor["Grey85"];
                gUIStyle.hover.background = texColor["Grey60"];
                gUIStyle.active.background = texColor["White"];

                gUIStyle.onNormal.textColor = Color.black;

                gUIStyle.onNormal.background = texColor["White"];
                gUIStyle.onHover.background = texColor["Grey60"];
                gUIStyle.onActive.background = texColor["Grey85"];

                gUIStyle.focused.background = texColor["TeamRed"];

                gUIStyle.stretchHeight = true;
                return gUIStyle;
            }
        }

        public static GUIStyle ActButton
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.button)
                {
                    fontSize = 24,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter,
                };

                gUIStyle.normal.textColor = Color.white;
                gUIStyle.hover.textColor = Color.white;
                gUIStyle.active.textColor = Color.white;

                gUIStyle.normal.background = texColor["MintGreen"];
                gUIStyle.hover.background = texColor["LMintGreen"];
                gUIStyle.active.background = texColor["DarkGreen"];

                gUIStyle.onNormal.textColor = Color.black;

                gUIStyle.onNormal.background = texColor["White"];
                gUIStyle.onHover.background = texColor["Grey60"];
                gUIStyle.onActive.background = texColor["Grey85"];

                gUIStyle.stretchHeight = true;
                return gUIStyle;
            }
        }
        public static GUIStyle DeactButton
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.button)
                {
                    fontSize = 24,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter,
                    clipping = TextClipping.Clip,
                };

                gUIStyle.normal.textColor = Color.white;
                gUIStyle.hover.textColor = Color.white;
                gUIStyle.active.textColor = Color.white;

                gUIStyle.normal.background = texColor["RedPink"];
                gUIStyle.hover.background = texColor["LPink"];
                gUIStyle.active.background = texColor["DarkPink"];

                gUIStyle.onNormal.textColor = Color.black;

                gUIStyle.onNormal.background = texColor["White"];
                gUIStyle.onHover.background = texColor["Grey60"];
                gUIStyle.onActive.background = texColor["Grey85"];

                gUIStyle.stretchHeight = true;
                return gUIStyle;
            }
        }

        public static GUIStyle SliderThumbStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    fontSize = 24,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter,
                };
                gUIStyle.stretchHeight = true;
                gUIStyle.fixedWidth = 40;
                Texture2D color = texColor["Black"];
                gUIStyle.normal.background = color;
                gUIStyle.hover.background = color;
                gUIStyle.active.background = color;
                gUIStyle.focused.background = color;
                return gUIStyle;
            }

        }

        public static GUIStyle SliderBackgroundStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    stretchWidth = true,
                    padding = new RectOffset(2, 2, 2, 2),
                    margin = new RectOffset(0, 0, 0, 0),
                };
                Texture2D color = texColor["White"];
                gUIStyle.normal.background = color;
                gUIStyle.hover.background = color;
                gUIStyle.active.background = color;
                gUIStyle.focused.background = color;
                return gUIStyle;
            }
        }

        public static GUIStyle PartnerSelectHeader
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 24,
                    fixedWidth = 200,
                    alignment = TextAnchor.MiddleCenter,
                    stretchHeight = true,
                };
                gUIStyle.normal.textColor = Color.white;
                return gUIStyle;
            }
        }

        static Texture2D ColorToTexture2D(Color32 color)
        {
            Texture2D texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture2D.SetPixel(0, 0, color);
            texture2D.Apply();
            return texture2D;
        }

    }
}
