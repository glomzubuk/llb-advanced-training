﻿using System.Reflection;
using AdvancedTraining;

#region Assembly attributes
[assembly: AssemblyVersion(PluginInfos.PLUGIN_VERSION)]
[assembly: AssemblyTitle(PluginInfos.PLUGIN_NAME + " (" + PluginInfos.PLUGIN_GUID + ")")]
[assembly: AssemblyProduct(PluginInfos.PLUGIN_NAME)]
#endregion

namespace AdvancedTraining
{
    public static class PluginInfos
    {
        public const string PLUGIN_GUID = "uk.daioutzu.plugins.llb.AdvancedTraining";
        public const string PLUGIN_NAME = "AdvancedTraining";
        public const string PLUGIN_VERSION = "2.1.1";
    }
}
