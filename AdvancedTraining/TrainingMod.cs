﻿using LLHandlers;
using UnityEngine;

namespace AdvancedTraining
{
    class TrainingMod : MonoBehaviour
    {
        void OnDestroy()
        {
            AdvancedTraining.Instance.OnTrainingModeStart -= AdvancedTraining_OnTrainingModeStart;
            AdvancedTraining.Instance.OnTrainingModeEnd -= AdvancedTraining_OnTrainingModeEnd;

            AdvancedTraining.Instance.OnHitboxKeyDown -= OnHitboxKeyDown;
            AdvancedTraining.Instance.OnAngleDrawerKeyDown -= OnAngleDrawerKeyDown;
            AdvancedTraining.Instance.OnFastFallIndicatorKeyDown -= OnFastFallIndicatorKeyDown;
            AdvancedTraining.Instance.OnPartnerSelectKeyDown -= OnPartnerSelectKeyDown;
        }

        void Awake()
        {
            AdvancedTraining.Instance.OnTrainingModeStart += AdvancedTraining_OnTrainingModeStart;
            AdvancedTraining.Instance.OnTrainingModeEnd += AdvancedTraining_OnTrainingModeEnd;

            AdvancedTraining.Instance.OnHitboxKeyDown += OnHitboxKeyDown;
            AdvancedTraining.Instance.OnAngleDrawerKeyDown += OnAngleDrawerKeyDown;
            AdvancedTraining.Instance.OnFastFallIndicatorKeyDown += OnFastFallIndicatorKeyDown;
            AdvancedTraining.Instance.OnPartnerSelectKeyDown += OnPartnerSelectKeyDown;
        }

        private void OnPartnerSelectKeyDown(bool InGame)
        {
            // Training Partner
            if (gameObject.GetComponent<TrainingPartner>() != null)
            {
                Destroy(gameObject.GetComponent<TrainingPartner>());
                TrainingPartner.Instance = null;
            }
            else
            {
                TrainingPartner.Instance = gameObject.AddComponent<TrainingPartner>();
            }
        }

        private void OnFastFallIndicatorKeyDown(bool inGame)
        {
            if (inGame)
            {
                GameObject worldObj = World.instance.gameObject;
                if (worldObj.GetComponent<FastFallIndicator>())
                {
                    Destroy(worldObj.GetComponent<FastFallIndicator>());
                }
                else { worldObj.AddComponent<FastFallIndicator>(); }
            }
        }

        private void OnAngleDrawerKeyDown(bool inGame)
        {
            if (inGame)
            {
                GameObject gameObj = GameCamera.gameplayCam.gameObject;
                if (gameObj.GetComponent<AngleDrawer>())
                {
                    Destroy(gameObj.GetComponent<AngleDrawer>());
                }
                else
                {
                    gameObj.AddComponent<AngleDrawer>();
                }
            }
        }

        private void OnHitboxKeyDown(bool inGame)
        {
            if (inGame)
            {
                GameObject gameObj = GameCamera.gameplayCam.gameObject;
                if (gameObj.GetComponent<HitboxMod>())
                {
                    Destroy(gameObj.GetComponent<HitboxMod>());
                }
                else { gameObj.AddComponent<HitboxMod>(); }
            }
        }

        private void AdvancedTraining_OnTrainingModeEnd()
        {
            Destroy(this);
            AdvancedTraining.Log.LogDebug("OnTrainingModeEnd");
        }

        private void AdvancedTraining_OnTrainingModeStart()
        {
            AdvancedTraining.Log.LogDebug("OnTrainingModeStart");
        }

        void Update()
        {
            if (AdvancedTraining.InGame)
            {
                StateSystemAct();
                CreateCharacterInfo();
            }
        }
        void LateUpdate()
        {
            // Player.Get(1) - Destroy if Player 2 "inMatch" && "JoinedMatch is false
            ALDOKEMAOMB player2 = ALDOKEMAOMB.BJDPHEHJJJK(1);
            //Player.inMatch = ALDOKEMAOMB.NGLDMOLLPLK
            if (!player2.NGLDMOLLPLK && !player2.PNHOIDECPJE && gameObject.GetComponent<TrainingPartner>() != null)
            {
                Destroy(gameObject.GetComponent<TrainingPartner>());
            }
        }

        static void StateSystemAct()
        {
            // State System - Pause Time, Frame Advance, SaveState and LoadState
            GameObject worldObj = World.instance.gameObject;
            if (AdvancedTraining.InGame && !AdvancedTraining.IsOnline)
            {
                if (worldObj.GetComponent<StateSystem>() == null)
                {
                    worldObj.AddComponent<StateSystem>();
                }
            }
        }

        static void CreateCharacterInfo()
        {
            GameObject worldObj = World.instance.gameObject;
            if (AdvancedTraining.InGame && !AdvancedTraining.IsOnline)
            {
                if (worldObj.GetComponent<CharacterInfo>() == null)
                {
                    worldObj.AddComponent<CharacterInfo>();
                }
            }
        }

        static Vector2 showVector = new Vector2(500, 430);
        static Vector2 hideVector = new Vector2(250, 60);
        Rect infoShowRect = new Rect(new Vector2(5, 5), hideVector);
        GUI.WindowFunction infoSmallWindow = new GUI.WindowFunction(InfoWindowSmall);
        GUI.WindowFunction infoWindow = new GUI.WindowFunction(InfoWindow);

        void OnGUI()
        {
            ConvertTo.ResolutionScale();
            if (AdvancedTraining.CurrentGameState == JOFJHDJHJGI.ABGFICGIPAD) //Training Mode Lobby
            {
                trainingPartnerReminder = GUILayout.Window(234532343, trainingPartnerReminder, PartnerWindow(), "", ATStyle.BlueWindowStyle);
            }
            if (AdvancedTraining.InGame && GameCamera.instance.GetComponent<Camera>().enabled == true)
            {
                bool showInfo = DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.LGILIJKMKOD;
                GUI.skin = null;
                infoShowRect.size = showInfo ? showVector : hideVector;
                infoShowRect = ConvertTo.ClampWindowScaled(infoShowRect);
                if (showInfo)
                {
                    infoShowRect = GUILayout.Window(9999, infoShowRect, infoWindow, GUIContent.none, ATStyle.BlueWindowStyle);
                } else if (AdvancedTraining.Instance.ShowSmallInfo.Value)
                {
                    infoShowRect = GUILayout.Window(9999, infoShowRect, infoSmallWindow, GUIContent.none, ATStyle.BlueWindowStyle);
                }
            }
        }
        static void InfoWindowSmall(int wId)
        {
            GUIStyle titleStyle = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 14,
            };

            GUILayout.Label("Advanced Training", ATStyle.HeaderStyle);
            GUILayout.BeginVertical(ATStyle.HeaderAreaStyle);
            GUILayout.FlexibleSpace();
            GUILayout.Label($"Pause to show controls", titleStyle);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }

        static void InfoWindow(int wId)
        {
            AdvancedTraining ins = AdvancedTraining.Instance;
            bool usingKB = InputHandler.GetLastActiveController() == Controller.mouseKeyboard;
            string titleTxt = string.Format($"Pause to hide controls");

            GUIStyle infoLblSyl = new GUIStyle(GUI.skin.label)
            {
                fontSize = 15,
                padding = new RectOffset(6, 0, 2, 0),
                alignment = TextAnchor.MiddleLeft
            };
            GUIStyle titleStyle = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 14,
            };
            titleStyle.normal.textColor = infoLblSyl.normal.textColor = Color.white;

            titleStyle.alignment = TextAnchor.LowerCenter;
            GUILayout.Label("Advanced Training", ATStyle.HeaderStyle);
            GUILayout.BeginVertical(ATStyle.HeaderAreaStyle);
            GUILayout.BeginVertical();
            GUILayout.Label($"Hitboxes [<b>{ins.HitboxKey.Value}</b>]: {TextHandler.Get(GameCamera.gameplayCam.GetComponent<HitboxMod>() ? "TAG_ON" : "TAG_OFF")}", infoLblSyl);
            GUILayout.Label($"Angle Drawer [<b>{ins.angleDrawerKey.Value}</b>]: {TextHandler.Get(GameCamera.gameplayCam.GetComponent<AngleDrawer>() ? "TAG_ON" : "TAG_OFF")}", infoLblSyl);
            GUILayout.Label($"Fast Fall Indicator [<b>{ins.FastFallIndicatorKey.Value}</b>]: {TextHandler.Get(World.instance.gameObject.GetComponent<FastFallIndicator>() ? "TAG_ON" : "TAG_OFF")}", infoLblSyl);
            //GUILayout.Label(usingKB ? $"Quick Reset [<b>{quickRestart}</b>]" : $"Quick Reset [<b>{modifierKey} + {inputnames["Taunt"]}</b>]", infoLblSyl);
            GUILayout.Label(usingKB ? $"Quick Reset [<b>{ins.QuickRestart.Value}</b>]" : $"Quick Reset [<b>{ins.modifierKeyName} + Taunt</b>]", infoLblSyl);
            GUILayout.Label($"Pause Time [<b>{ins.PauseTimeKey.Value}</b>]", infoLblSyl);
            GUILayout.Label($"Advance Frame [<b>{ins.AdvanceFrame.Value}</b>]", infoLblSyl);
            GUILayout.Label($"Save State [<b>{ins.SaveStateKey.Value}</b>]", infoLblSyl);
            GUILayout.Label($"Load State [<b>{ins.LoadStateKey.Value}</b>]", infoLblSyl);
            GUILayout.Label($"Stick Ball to Player [<b>{ins.modifierKeyName} + Bring It!</b>]", infoLblSyl);
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.Label(titleTxt, titleStyle);
            GUILayout.EndVertical();
        }

        Rect trainingPartnerReminder = new Rect(new Vector2(1920 - 675, 0), new Vector2(480, 80f));
        GUI.WindowFunction PartnerWindow()
        {
            if (gameObject.GetComponent<TrainingPartner>() != null)
            {
                return new GUI.WindowFunction(TrainingPartner.Instance.PartnerSelect);
            }
            else
            {
                return new GUI.WindowFunction(TrainingPartnerReWindow);
            }
        }

        void TrainingPartnerReWindow(int wId)
        {
            GUIStyle largeText = new GUIStyle()
            {
                fontSize = 20,
                alignment = TextAnchor.MiddleCenter,
            };
            largeText.normal.textColor = Color.white;

            GUILayout.BeginVertical(ATStyle.AreaStyle);
            GUILayout.FlexibleSpace();
            GUILayout.Label($"Activate Training Partner: [<b>{AdvancedTraining.Instance.PartnerSelectKey.Value}</b>]", largeText);
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        }
    }
}
