# AdvancedTraining
Version: 2.1.0  
Authors: Daioutzu, MrGentle, Leibniz, Mecha, Axolotl, Glomzubuk  
Release Data: 28/10/20

This mod was made to give players a better experience when playing in Training mode in Lethal League Blaze.

## Features include:
- Hitbox Viewer  
- Fastfall indicator  
- Angle Drawer  
- Frame Advance  
- Training Partner  

## Changelogs   
### v2.0.1:
#### Training Partner:  
- Added new setting to remove an angle from the "Random" Angle Mode.  
- Fixed "Intangible Partner" option from affecting the player.  
- Few UI tweaks and code clean up

### v2.0.0
#### Angle Drawer:  
- Trace ball follow through can now be set to 1-10 Bounces.  
- Added "Options" to customise each angles reflect amount.  

#### Corpse Spawner:  
- Made Standalone: https://github.com/Daioutzu/LLBMM-CorpseJuggler/releases

#### Hitbox Mod:  
- Ball Hitbox changes colour depending on if you can Parry or Special  
	Normal: Blue.  
	Can Parry: Yellow.  
	Can Special: Cyan. Has Priority over "Can Parry"  
- Added Full Charge indicator as white swing hitbox.  
- Fixed Corpses not showing a hitbox.  

#### Training Partner:  
- Added "Training Partner" window to Trainig lobby for easier visibility of the feature.  
- Partner's Character is now visible in the lobby.  
- Added "Random" as a possible character pick.  
- Fixed the possiblity of matching skins. Training sessions will now be less awkward.  
- Fixed Training partner not leaving when you disabled the mod. It just wanted to be noticed UwU.  
- "Can't hit partner" renamed to "Intangible Partner"  
- "No Gravity" renamed to "Fixed Air Position"  
- Partner no longer gets hit even if "Can't hit partner" was on.  
- Added "None" as a Swing Type.  
- Added "Swing Spammer" mode to 'better train against online opponents' ;)  
- Training Partner just learned how to parry with frame levels of accuracy though doesn't go for the instant parry.  
- 100% chance of parry, but is whiling to reduce it by 10% increments.  

#### State System:  
- Added Quick Restart option for Gamepad. (Select + Taunt)  
- Pause time is now a toggle  
- Continoius frame advance while Holding the "Advance Frame" key  
- Fixed Frame Advance, ect from being used online.  

#### General:  
- Added "Character Info" window with:  
	- Hurt Boxes  
	- Hit Boxes  
	- Angles  
	- Movment  
	- Current Ability  
	- Player Info  
	- Options  
	- All Ability States  
	- Save States  
- Added All keybinds to the "Advanced Training" control menu.  
- GUI now scales with screen Size.  
- Some UI Changes  
